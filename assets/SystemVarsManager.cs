﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Linq;

public class SystemVarsManager : MonoBehaviour
{
    // THIS SCRIPT MANAGES THE SUBMISSION AND RETRIEVAL OF SYSTEM VARIABLES THAT CAN BE TWEAKED AND ADJUSTED
    // WHILE THE BUILD IS IN THE WILD, WITHOUT THE NEED TO RECOMPILE THE APP BINARY, ETC.

    // THE SYSTEM IS FULLY EXTENDABLE WITH NEW VARIABLES BEING ABLE TO BE CREATED WITH AN API CALL.
    // ALL VALUES ON THE SERVER SIDE ARE STORED AS STRINGS. ON THE CLIENT SIDE THEY ARE CONVERTED TO A NUMBER OF
    // OTHER MORE USEFUL FORMATS.

    public static SystemVarsManager instance;

    public enum VarType
    {
        Flag,
        Int,
        IntArray,
        StringArray,
        String
    }

    // MATCH TYPE BELOW TO ENUM ENTRY
    public enum VariablesList
    {
        disableNativeAds,               // 0
        disableInterstitialAds,         // 1
        disableRewardAds,               // 2
        watchAdEarnedTokens,            // 3
        startingCurrency,               // 4
        disableIAPTokens,               // 5
        IAPNumSKUSToShow,               // 6
        tier1IAPcurrency,               // 7
        tier1IAPSKUs,                   // 8
        adminList,                      // 9
        trialPeriodDays,                // 10
        _PopTitleNewPlayerGrace,        // 11
        _PopBodyNewPlayerGrace,         // 12
        _PopButton1NewPlayerGrace,      // 13
        _PopButton2NewPlayerGrace,      // 14
        _PopBodyKickPlayer,             // 15
        _PopBodyPinChanged,             // 16
        playTurnHours,                  // 17
        voteTurnHours,                  // 18
        earlyBirdHoursPlay,             // 19
        earlyBirdHoursVote,             // 20
        whitelistMembers,               // 21
        iOSUnitNative_Test,             // 22
        iOSUnitNative,                  // 23
        iOSUnitReward_Test,             // 24
        iOSUnitReward,                  // 25
        iOSUnitInterstitial_Test,       // 26
        iOSUnitInterstitial,            // 27
        useTestAdUnits,                 // 28
        memberJoinTokens,               // 29
        numPlayers_Min,                 // 30
        numPlayers_Max,                 // 31
        memberWinRound,                 // 32
        memberWinPlaceGame,             // 33
        memberEarlybird,                // 34
        memberEarnOnVote,               // 35
        avatarAPI,                      // 36
        AppURLGateway,                  // 37
        InvitationText,                 // 38
        TestInterstitials,              // 39
        SwapsPerRoundLimit,             // 40
        CAv_BrightnessMin,              // 41
        CAv_BrightnessMax,              // 42
        CAv_ContrastMin,                // 43
        CAv_ContrastMax,                // 44
        TestNotificationsFlow,          // 45
        FavoritesMaxMember,             // 46
        FavoritesMaxNonMember,          // 47
        TestNewGameInHours,             // 48
        PointsForVictory1Player,        // 49
        PointsForVictory4Player,        // 50
        PointsForVictory5Player,        // 51
        PointsForVictory6Player,        // 52
        PointsForVictory7Player,        // 53
        PointsForVictory8Player,        // 54
        AndroidUnitNative_Test,         // 55
        AndroidUnitNative,              // 56
        AndroidUnitReward_Test,         // 57
        AndroidUnitReward,              // 58
        AndroidUnitInterstitial_Test,   // 59
        AndroidUnitInterstitial,        // 60
        CallTimeoutSecondsThreshold,    // 61
        EnableMWDebugging,              // 62
        DisableIAPInit,                 // 63
        DisableSwapping,                // 64
        showAdsToMembers,               // 65
        MinimumBuildNum,                // 66
        EnableMWLogging,                // 67
        OutOfDateMessage,               // 68
        OutOfDateUpdateList,            // 69
        DisableNeutering,               // 70
        NUM_IN_VARIABLES_LIST
    }

    private VarType[] VarTypes = new VarType[71]
    {
        VarType.Flag,               // 0
        VarType.Flag,               // 1
        VarType.Flag,               // 2
        VarType.Int,                // 3
        VarType.Int,                // 4
        VarType.Flag,               // 5
        VarType.Int,                // 6
        VarType.IntArray,           // 7
        VarType.IntArray,           // 8
        VarType.StringArray,        // 9
        VarType.Int,                // 10
        VarType.String,             // 11
        VarType.String,             // 12
        VarType.String,             // 13
        VarType.String,             // 14
        VarType.String,             // 15
        VarType.String,             // 16
        VarType.Int,                // 17
        VarType.Int,                // 18
        VarType.Int,                // 19
        VarType.Int,                // 20
        VarType.StringArray,        // 21
        VarType.String,             // 22
        VarType.String,             // 23
        VarType.String,             // 24
        VarType.String,             // 25
        VarType.String,             // 26
        VarType.String,             // 27
        VarType.Flag,               // 28
        VarType.Int,                // 29
        VarType.Int,                // 30
        VarType.Int,                // 31
        VarType.Int,                // 32
        VarType.Int,                // 33
        VarType.Int,                // 34
        VarType.Int,                // 35
        VarType.String,             // 36
        VarType.String,             // 37
        VarType.String,             // 38
        VarType.Flag,               // 39
        VarType.Int,                // 40
        VarType.Int,                // 41
        VarType.Int,                // 42
        VarType.Int,                // 43
        VarType.Int,                // 44
        VarType.Flag,               // 45
        VarType.Int,                // 46
        VarType.Int,                // 47
        VarType.Int,                // 48
        VarType.Int,                // 49
        VarType.Int,                // 50
        VarType.Int,                // 51
        VarType.Int,                // 52
        VarType.Int,                // 53
        VarType.Int,                // 54
        VarType.String,             // 55
        VarType.String,             // 56
        VarType.String,             // 57
        VarType.String,             // 58
        VarType.String,             // 59
        VarType.String,             // 60
        VarType.Int,                // 61
        VarType.Flag,               // 62
        VarType.Flag,               // 63
        VarType.Flag,               // 64
        VarType.Flag,               // 65
        VarType.Int,                // 66
        VarType.Flag,               // 67
        VarType.String,             // 68
        VarType.String,             // 69
        VarType.Flag                // 70
    };

    [SerializeField] private List<RESTInterface.Variable> validVariablesList;
    private RESTInterface.Variable[] validVariables;
    [SerializeField] private bool updateVariable;
    [SerializeField] private UpdateVar updateVar;

    [System.Serializable]
    public class UpdateVar
    {
        public VariablesList keyEnum;
        public string value;
    }

    // ----------------------------------------
    // EVENT REGISTRATION
    void OnEnable() { RegisterEvents(true); }
    void OnDisable() { RegisterEvents(false); }
    void OnDestroy()
    {
        RegisterEvents(false);
    }
    private void RegisterEvents(bool register)
    {
        if (register)
        {
            RESTInterface.BroadcastSystemVariables += SystemVariablesListener;
        }
        else
        {
            RESTInterface.BroadcastSystemVariables -= SystemVariablesListener;
        }
    }
    // ----------------------------------------

    private void Awake()
    {
        instance = this;
    }

    private VariablesList currentDropdownSetting = VariablesList.NUM_IN_VARIABLES_LIST;
#if UNITY_EDITOR
    private void Update()
    {
        if( updateVar.keyEnum != currentDropdownSetting )
        {
            currentDropdownSetting = updateVar.keyEnum;
            updateVar.value = GetString(updateVar.keyEnum, true);
        }

        if (updateVariable)
            StartCoroutine(DoUpdateSystemVariable());
    }
#endif

    private IEnumerator DoUpdateSystemVariable()
    {
        updateVariable = false;
        bool newValueValidated = ValidateNewValue(updateVar);

        if( newValueValidated )
        {
            Debug.Log("Variable update attempt: " + updateVar.keyEnum.ToString() + ", " + updateVar.value);
            yield return StartCoroutine(RESTInterface.instance.PostSystemVariable(updateVar.keyEnum.ToString(), updateVar.value));
            StartCoroutine(RESTInterface.instance.GetSystemVariables());
        }
        else
            Globals.MemeLog(MethodBase.GetCurrentMethod(), "Bad value attempted for " + updateVar.keyEnum.ToString(), true, Globals.LogMethod.Error);
    }

    private bool ValidateNewValue(UpdateVar updateVar)
    {
        if (updateVar.value == "") return false;

        int index = 0;
        for( int i = 0; i < (int)VariablesList.NUM_IN_VARIABLES_LIST; i++ )
        {
            if( updateVar.keyEnum == (VariablesList)i )
            {
                index = i;
                break;
            }    
        }
        VarType myType = VarTypes[index];

        switch( myType )
        {
            case VarType.String:
                return true;
            case VarType.Flag:
                return updateVar.value == "0" || updateVar.value == "1";
            case VarType.Int:
                int number;
                if (Int32.TryParse(updateVar.value, out number))
                    return true;
                else
                    return false;
            case VarType.IntArray:
                string[] intValues_Str = GetArrayStr(updateVar.value, ',');

                if (intValues_Str != null)
                {
                    for (int i = 0; i < intValues_Str.Length; i++)
                    {
                        if (Int32.TryParse(intValues_Str[i], out _)) { }
                        else
                            return false;
                    }
                    return true;
                }
                else
                    return false;
            case VarType.StringArray:
                string[] stringValues_Str = GetArrayStr(updateVar.value, ',');
                return stringValues_Str != null;
        }
        return false;
    }

    private string[] GetArrayStr(string value, char splitChar)
    {
        string[] split = value.Split(splitChar);
        return split.Length > 0 ? split : null;
    }

    void SystemVariablesListener(RESTInterface.Variable[] variables)
    {
        Globals.DisabledBeenSaid = false;
        validVariables = new RESTInterface.Variable[(int)VariablesList.NUM_IN_VARIABLES_LIST];

        for( int i = 0; i < (int)VariablesList.NUM_IN_VARIABLES_LIST; i++ )
        {
            for( int j = 0; j < variables.Length; j++ )
            {
                if (((VariablesList)i).ToString() == variables[j].key)
                {
                    validVariables[i] = variables[j];
                    break;
                }     
            }
        }
        ToList();
    }

    private void ToList()
    {
        validVariablesList.Clear();
        for( int i = 0; i < validVariables.Length; i++ )
        {
            if( validVariables[i] != null )
            {
                if (validVariables[i].key != "")
                    validVariablesList.Add(validVariables[i]);
            }
        }
    }

    // RETRIEVAL
    private int? GetIndex(VariablesList vName)
    {
        for (int i = 0; i < validVariablesList.Count; i++)
        {
            if (validVariablesList[i].key == vName.ToString())
                return i;
        }
        return null;
    }

    public bool? GetFlag(VariablesList vName)
    {
        int? index = GetIndex(vName);
        if (index != null)
            return validVariablesList[(int)index].value == "1" || validVariablesList[(int)index].value.ToLower() == "true";
        else
            return null;
    }

    public int? GetInt(VariablesList vName)
    {
        int? index = GetIndex(vName);
        if (index != null)
            return Convert.ToInt32(validVariablesList[(int)index].value);
        else
            return null;
    }

    public int?[] GetIntArray(VariablesList vName)
    {
        int? index = GetIndex(vName);
        if( index != null )
        {
            string[] split = GetArrayStr(validVariablesList[(int)index].value, ',');
            int?[] array = new int?[split.Length];
            for(int i = 0; i < split.Length; i++ )
                array[i] = Convert.ToInt32(split[i]);
            return array;
        }
        else
            return null;
    }

    public string[] GetStringArray(VariablesList vName)
    {
        int? index = GetIndex(vName);
        if (index != null)
        {
            string[] split = GetArrayStr(validVariablesList[(int)index].value, ',');
            return split;
        }
        return null;
    }

    public string GetString(VariablesList vName, bool forEditorField = false)
    {
        int? index = GetIndex(vName);
        if (index != null)
        {
            string value = validVariablesList[(int)index].value;
            return value;
        }    
        return null;
    }

    public int GetVariableCount()
    {
        return validVariablesList.Count;
    }

    public static VariablesList GetVariable(string str)
    {
        for( int i = 0; i < (int)VariablesList.NUM_IN_VARIABLES_LIST; i++ )
        {
            if( str == ((VariablesList)i).ToString() )
                return (VariablesList)i;
        }
        Debug.LogError("NO VARIABLE FOUND FOR STRING. PLEASE FIX. string=" + str);
        return VariablesList.NUM_IN_VARIABLES_LIST;
    }
}
