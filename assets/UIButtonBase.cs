﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIButtonBase : Button {

    [SerializeField] protected Image buttonBacking;
    [SerializeField] protected bool becomeAvailable = true;
    [SerializeField] protected PresentationCallToAction[] pulseElements;
    protected bool highlightState = false;
    [SerializeField] protected SoundManager.SoundElementNames playSound;
    [SerializeField] protected bool repetitiveUse = false;
    [SerializeField] protected bool pulseAnimation = false;

    private TextMeshProUGUI[] textChildren;

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // EVENT LISTENERS
    protected override void OnEnable() { RegisterStates(true); }
    protected override void OnDisable()
    {
        base.OnDisable();
        RegisterStates(false);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        RegisterStates(false);
    }
    private void RegisterStates(bool register)
    {
        if (register)
        {
            base.OnEnable();
            LoadingCircle.LoadCircleToggle += LoadCircleToggleListener;
            SetAvailable();
        }
        else
        {
            LoadingCircle.LoadCircleToggle -= LoadCircleToggleListener;
        }
    }
    //::::::::::::::::::::::::::::::::::::::::::::::::::::::

    public void SetBecomeAvailable(bool value)
    {
        becomeAvailable = value;
    }

    protected override void Awake()
    {
        base.Awake();
        GetTextChildren();

        if( this.gameObject.activeInHierarchy )
            StartCoroutine(SetColors());
    }

    private IEnumerator SetColors()
    {
        while (Presentation.instance == null)
            yield return null;
        ColorBlock colorBlock = this.colors;
        colorBlock.disabledColor = Presentation.instance.nonInteractableUIColor;
        this.colors = colorBlock;
    }

    private void GetTextChildren()
    {
        textChildren = this.gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        //Debug.Log("num of uiButtonBase textChildren for " + this.gameObject.name + ": " + textChildren.Length);
    }

    protected override void Start()
    {
        //base.Start();
        this.onClick.AddListener(TaskOnClick);

        if (buttonBacking != null)
        {
            PresentationButtonBacking pBB = buttonBacking.GetComponent<PresentationButtonBacking>();
            if (pBB != null)
                DestroyImmediate(pBB);
        }
    }

    protected virtual void TaskOnClick()
    {
        highlightState = true;
        if (buttonBacking != null)
            buttonBacking.color = Color.green;

        if (this.gameObject.activeInHierarchy)
            StartCoroutine(WaitThenGoBack());

        HandlePulseElements();

        SoundManager.instance.Play(playSound);

        Animator anim = null;
        if( buttonBacking != null )
            anim = buttonBacking.gameObject.GetComponent<Animator>();
        if (anim != null)
            anim.SetTrigger("noAnim");
    }

    protected void SetAvailable()
    {
        highlightState = false;
        if( buttonBacking != null )
            buttonBacking.color = Color.white;
    }

    private IEnumerator WaitThenGoBack()
    {
        yield return new WaitForSeconds(0.75f);
        if( buttonBacking != null )
            buttonBacking.color = !repetitiveUse ? Presentation.instance.nonInteractableButtonColor : Color.white;
        highlightState = false;
    }

    void LoadCircleToggleListener(bool value)
    {
        if (this.gameObject.activeInHierarchy)
            StartCoroutine(WaitThenSolveListener(value));
    }

    IEnumerator WaitThenSolveListener(bool value)
    {
        //if (this.gameObject.name.ToLower().Contains("trythisone"))
        //    print("see it: " + becomeAvailable);

        yield return null;
        if (becomeAvailable && !highlightState)
        {
            Color toColor = value ? Presentation.instance.nonInteractableButtonColor : Color.white;
            float tweenTime = value ? 0.05f : 0.4f;
            buttonBacking.DOColor(toColor, tweenTime);
        }
    }

    private void HandlePulseElements()
    {
        if( pulseElements.Length > 0 )
        {
            for( int i = 0; i < pulseElements.Length; i++ )
            {
                if( pulseElements[i] != null )
                    pulseElements[i].ButtonSelect();
            }
                
        }
    }

    public void ShowHide(bool value)
    {
        if (value)
            Show();
        else
            Hide();
    }

    public void Show()
    {
        //Debug.Log(this.gameObject.name + " set to Show. " + Time.time);
        if (buttonBacking == null)
            Debug.LogError("No buttonBacking set for button: " + this.gameObject.name);
        else
        {
            buttonBacking.gameObject.SetActive(true);
            buttonBacking.color = Color.white;

            if (pulseAnimation)
            {
                if (IsInteractable())
                    SetPulseAnim(true);
            }
        }
        this.interactable = true;
    }

    public void SetPulseAnim(bool value)
    {
        Animator anim = buttonBacking.gameObject.GetComponent<Animator>();
        if( anim != null )
        {
            string animTrigger = value ? "pulseAnim" : "noAnim";
            anim.SetTrigger(animTrigger);
        }
    }

    public void Hide()
    {
        //Debug.Log(this.gameObject.name + " set to Hide. " + Time.time);
        buttonBacking.gameObject.SetActive(false);
        if (pulseAnimation)
            SetPulseAnim(false);
    }

    public virtual void SetInteractable(bool value)
    {
        if (Presentation.instance == null)
            return;

        this.interactable = value;
        Color toColorForBacking = value ? Color.white : Presentation.instance.nonInteractableButtonColor;
        Color toColorForUI = value ? Presentation.instance.primaryUIColor : Presentation.instance.nonInteractableUIColor;
        buttonBacking.color = toColorForBacking;
        this.image.color = toColorForUI;

        if (textChildren != null)
        {
            for (int i = 0; i < textChildren.Length; i++)
                textChildren[i].color = toColorForUI;
        }

        if( pulseAnimation )
            SetPulseAnim(value);
    }

    public void SetBackingColor(Color color)
    {
        buttonBacking.color = color;
    }
}
