﻿// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// THIS IS THE MAIN STATIC UTILITY CLASS FOR GENERAL PURPOSE DATA WRANGLING
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using Amazon.S3;
using Amazon.S3.Model;
using UnityEngine.Networking;
using System.Linq;

public static class DataManager
{

    private const string robot_str = "Robot";
    private const string currencyNameSingular_str = "Token";
    private const string currencyName_str = "Tokens";
    private const string roomName_str = "Room";

    public enum NumberText
    {
        Zero,           // 0
        One,            // 1
        Two,            // 2
        Three,          // 3
        Four,           // 4
        Five,           // 5
        Six,            // 6
        Seven,          // 7
        Eight,          // 8
        Nine,           // 9
        Ten,            // 10
        Eleven,         // 11
        Twelve,         // 12
        Thirteen,       // 13
        Fourteen,       // 14
        Fifteen,        // 15
        NUM_OF_VALUES   // 16
    }

    public static string GetTextForInteger(int value)
    {
        if (value >= (int)NumberText.NUM_OF_VALUES)
            return "bad";
        else
            return ((NumberText)value).ToString();
    }

    public enum CoreFlow
    {
        Standard,
        ShowASummary,
        FinalVoteOfRound
    }

    private static CoreFlow coreFlow = CoreFlow.Standard;

    public static CoreFlow GetCoreFlow()
    {
        return coreFlow;
    }

    public static void SetCoreFlow(CoreFlow flow)
    {
        Globals.MemeLog(MethodBase.GetCurrentMethod(), "CoreFLow set to: " + flow.ToString() + "  " + Time.time);
        coreFlow = flow;
    }

    public static Room StoreGameRoomOut(RESTInterface.GameRoomOut gameRoomOut)
    {
        Room room = new Room
        {
            id = gameRoomOut.id,
            name = gameRoomOut.name,
            displayName = gameRoomOut.displayName,
            owner = gameRoomOut.owner,
            games = new List<RESTInterface.GameSummaryOut>(),
            fullGames = new List<RESTInterface.GameSummaryOut>(),
            pin = gameRoomOut.code
        };
        if (gameRoomOut.roomPlayers == null)
            room.roomPlayers = new List<RESTInterface.RoomPlayerOut>();
        else
        {
            if (gameRoomOut.roomPlayers.Length > 0)
                room.roomPlayers = SetupMembersListInRoom(gameRoomOut);
            else
                room.roomPlayers = new List<RESTInterface.RoomPlayerOut>();
        }

        if( gameRoomOut.games.Length > 0 )
        {
            room.games = SetupGameListInRoom(gameRoomOut);
            room.games = SortGamesByStarted(room.games);

            room.fullGames = SetupGameListInRoom(gameRoomOut, false);
            room.fullGames = SortGamesByStarted(room.fullGames);

            if (room.games.Count > 0)
            {
                RESTInterface.GameSummaryOut game = DataManager.GetCurrentGame(room);
                if (DataManager.GameHasWinner(game))
                    room.status = Room.Status.Complete;

                room.lastWrite = game.modified;
                room.questionStarted = game.currentRound.questionStarted;
                room.questionElapsed = game.currentRound.questionElapsed;
                room.voteStarted = game.currentRound.voteStarted;
                room.voteElapsed = game.currentRound.voteElapsed;
            }
        }
        else
            room.games = new List<RESTInterface.GameSummaryOut>();

        room.chatEntries = gameRoomOut.chatter;

        return room;
    }

    public static List<Room> RetrieveGameRooms(RESTInterface.GameRoomOut[] gameRoomsOut)
    {
        List<Room> gameRoomList = new List<Room>();

        if( gameRoomsOut != null )
        {
            for (int i = 0; i < gameRoomsOut.Length; i++)
            {
                Room room = StoreGameRoomOut(gameRoomsOut[i]);
                gameRoomList.Add(room);
            }
        }

        return gameRoomList;
    }

    public static bool GetGameCompleteButPastDue(RESTInterface.GameSummaryOut game, Room room = null)
    {
        // WE'RE JUST GONNA AVOID THIS FOR NOW
        return false;

        /*
        int hourThresh = Globals.gameCompletePastDueThreshold;

        TimeSpan ts = DataManager.GetTimeSinceVictory(game);
        double h = ts.TotalHours;
        int hours = Mathf.RoundToInt((float)h);
        if (room != null)
            Debug.Log("Past Due check for " + room.name + " is: " + hours + " hours. Threshold is: " + hourThresh);
        return hours > hourThresh;
        */
    }

    public static Room.Status GetRoomStatus(Room room)
    {
        if (room.games == null)
            return Room.Status.Staging;

        if (room.games.Count == 0)
            return Room.Status.Staging;
        else
        {
            // THIS IS GETTING THE WRONG GAME
            RESTInterface.GameSummaryOut currentGame = GetCurrentGame(room);

            bool hasWinner = GameHasWinner(currentGame);
            if (hasWinner)
            {
                bool pastDue = GetGameCompleteButPastDue(currentGame);
                return pastDue ? Room.Status.Staging : Room.Status.Complete;
            }

            else
                return Room.Status.Active;
        }
    }

    private static List<RESTInterface.RoomPlayerOut> SetupMembersListInRoom(RESTInterface.GameRoomOut inRoom)
    {
        List<RESTInterface.RoomPlayerOut> memberList = new List<RESTInterface.RoomPlayerOut>();
        for (int i = 0; i < inRoom.roomPlayers.Length; i++)
            memberList.Add(inRoom.roomPlayers[i]);
        return memberList;
    }

    private static List<RESTInterface.GameSummaryOut> SetupGameListInRoom(RESTInterface.GameRoomOut inRoom, bool handleDeletions = true)
    {
        List<RESTInterface.GameSummaryOut> gameList = new List<RESTInterface.GameSummaryOut>();
        //Debug.Log("SetupGameListInRoom, inRoom.games.Length: " + inRoom.games.Length);
        for (int i = 0; i < inRoom.games.Length; i++)
        {
            if( !GameWasDeleted( inRoom.games[i]) || !handleDeletions)
                gameList.Add(inRoom.games[i]);
        }
        return gameList;
    }

    public static Room.CurrentRoundState GetRoundState(RESTInterface.GameSummaryOut game)
    {
        if (game != null)
        {
            if (game.currentRound != null)
            {
                RESTInterface.RoundSummaryOut round = game.currentRound;
                if (round != null)
                {
                    int statusValue = round.status;
                    //Debug.Log("Switch Case for Round Status is being done here.");
                    switch (statusValue)
                    {
                        case 0:
                            return Room.CurrentRoundState.Answer;
                        case 1:
                            return Room.CurrentRoundState.Voting;
                        case 2:
                            return Room.CurrentRoundState.Complete;
                    }
                    return Room.CurrentRoundState.NoRound;
                }
                else
                    return Room.CurrentRoundState.NoRound;
            }
            else
                return Room.CurrentRoundState.NoRound;
        }
        else
            return Room.CurrentRoundState.NoRound;
    }

    public static Room.CurrentRoundState GetRoundState(RESTInterface.GameDetailOut game)
    {
        if (game != null)
        {
            //Debug.Log("game.rounds.Length: " + game.rounds.Length);
            if (game.rounds != null)
            {
                if (game.rounds.Length > 0)
                {
                    RESTInterface.RoundOut round = game.rounds[game.rounds.Length - 1];
                    if (round != null)
                    {
                        int statusValue = round.status;
                        switch (statusValue)
                        {
                            case 0:
                                return Room.CurrentRoundState.Answer;
                            case 1:
                                return Room.CurrentRoundState.Voting;
                            case 2:
                                return Room.CurrentRoundState.Complete;
                        }
                        return Room.CurrentRoundState.NoRound;
                    }
                    else
                        return Room.CurrentRoundState.NoRound;
                }
                else
                    return Room.CurrentRoundState.NoRound;
            }
            else return Room.CurrentRoundState.NoRound;
        }
        else
            return Room.CurrentRoundState.NoRound;
    }

    public static Room.CurrentRoundState GetRoundState(Room room)
    {
        RESTInterface.GameSummaryOut game = GetCurrentGame(room);
        return GetRoundState(game);
    }

    public static bool GetIsCurrentGame(RESTInterface.GameDetailOut gameDetailOut, Room room)
    {
        RESTInterface.GameSummaryOut currentSummaryOut = GetCurrentGame(room);
        return gameDetailOut.id == currentSummaryOut.id;
    }

    public static RESTInterface.GameSummaryOut GetCurrentGame(RESTInterface.GameRoomOut room)
    {
        if (room.games == null)
            return null;
        else
        {
            if (room.games.Length == 0)
                return null;
            else
            {
                if (room.games.Length == 1)
                {
                    if (room.games[0].players.Length > 0)
                        return room.games[0];
                    else
                        return null;
                }   
                else
                {
                    List<RESTInterface.GameSummaryOut> gameList = new List<RESTInterface.GameSummaryOut>();
                    for (int i = 0; i < room.games.Length; i++)
                        gameList.Add(room.games[i]);
                    return GetValidGameFromList(gameList);
                }     
            }
        }
    }

    public static long GetStasisLaunchTime(Room room)
    {
        RESTInterface.GameSummaryOut stasisGame = GetCurrentGame(room, true);
        if (stasisGame == null)
            return -1;
        else
        {
            if (stasisGame.players.Length > 0)
                return -1;
            else
                return stasisGame.started;
        }
    }

    public static bool GameWasDeleted(RESTInterface.GameSummaryOut game)
    {
        return game.deleted > 1;
    }

    private static RESTInterface.GameSummaryOut GetValidGameFromList(List<RESTInterface.GameSummaryOut> gameList, bool getStasisGame = false)
    {
        int topIndex = gameList.Count - 1;
        for (int i = topIndex; i >= 0; i-- )
        {
            if (gameList[i].players.Length > 0 || getStasisGame)
                return gameList[i];
        }
        return null;

        /*
        RESTInterface.GameSummaryOut zeroIndexGame = gameList[0];
        RESTInterface.GameSummaryOut lastIndexGame = gameList[gameList.Count - 1];

        bool isZeroIndex = true;
        RESTInterface.GameSummaryOut theGame = zeroIndexGame;
        if ( zeroIndexGame.started < lastIndexGame.started )
        {
            theGame = lastIndexGame;
            isZeroIndex = false;
        }

        if (theGame.players.Length > 0 || getStasisGame)
        {
            return theGame;
        }
        else
        {
            if( isZeroIndex )
            {
                if (gameList[1].players.Length > 0)
                    return gameList[1];
                else
                    return null;
            }
            else
            {
                if (gameList[gameList.Count - 2].players.Length > 0)
                    return gameList[gameList.Count - 2];
                else
                    return null;
            }
        }
        */
    }

    /*
    public static RESTInterface.GameSummaryOut[] RemoveGameDeletions(RESTInterface.GameSummaryOut[] gamesArray)
    {
        List<RESTInterface.GameSummaryOut> gameList = gamesArray.ToList();
        gameList = RemoveGameDeletions(gameList);
        return gameList.ToArray();
    }
    */

    public static List<RESTInterface.GameSummaryOut> RemoveGameDeletions(List<RESTInterface.GameSummaryOut> gameList)
    {
        List<RESTInterface.GameSummaryOut> gList = gameList;
        for ( int i = 0; i < gList.Count; i++ )
        {
            if (GameWasDeleted(gList[i]))
                gList.Remove(gList[i]);
        }
        return gList;
    }

    public static RESTInterface.GameSummaryOut GetCurrentGame(Room room, bool getStasisGame = false)
    {
        if (room == null)
            return null;

        if (room.games == null)
            return null;
        else
        {
            if (room.games.Count == 0)
                return null;
            else
            {
                if (room.games.Count == 1)
                {
                    if (room.games[0].players.Length > 0 || getStasisGame )
                        return room.games[0];
                    else
                        return null;
                }    
                else
                    return GetValidGameFromList(room.games, getStasisGame);
            }
        }

    }

    public static RESTInterface.RoundSummaryOut GetCurrentRound(Room room)
    {
        if (room == null)
            return null;
        RESTInterface.GameSummaryOut game = GetCurrentGame(room);
        return game.currentRound;
    }

    public static RESTInterface.RoundOut GetCurrentRound(RESTInterface.GameDetailOut gameDetailOut)
    {
        return (gameDetailOut.rounds[gameDetailOut.rounds.Length - 1]);
    }

    public static RESTInterface.RoundSummaryOut GetCurrentRound(RESTInterface.GameSummaryOut gameSummaryOut)
    {
        return (gameSummaryOut.currentRound);
    }

    public static RESTInterface.RoundOut GetPreviousRound(RESTInterface.GameDetailOut gameDetailOut)
    {
        return (gameDetailOut.rounds[gameDetailOut.rounds.Length - 2]);
    }

    public static bool GetRoundAnswered(RESTInterface.RoundOut round, RESTInterface.UserOut user = null)
    {
        string userEmail = Globals.email;

        if (round == null)
            return false;
        if (user != null)
            userEmail = user.email.ToLower();

        for (int i = 0; i < round.answers.Length; i++)
        {
            RESTInterface.AnswerOut currentAnswer = round.answers[i];
            if (currentAnswer.user.email.ToLower() == userEmail.ToLower())
                return true;
        }
        return false;
    }

    public static bool GetRoundAnswered(RESTInterface.RoundSummaryOut round, RESTInterface.UserSummaryOut user = null)
    {
        if (round == null)
        {
            Debug.Log("Round is null.");
            return false;
        }
        string userEmail = Globals.email;

        if (round == null)
            return false;
        if (user != null)
            userEmail = user.email.ToLower();

        if (round.answers == null)
            return false;

        for (int i = 0; i < round.answers.Length; i++)
        {
            RESTInterface.AnswerSummaryOut currentAnswer = round.answers[i];
            if (currentAnswer.user.email.ToLower() == userEmail.ToLower())
                return true;
        }
        return false;
    }

    public static bool GetRoundVoted(RESTInterface.RoundOut round, string email)
    {
        if (round != null)
        {
            if (round.votes != null)
            {
                if (round.votes.Length > 0)
                {
                    for (int i = 0; i < round.votes.Length; i++)
                    {
                        if (round.votes[i].castUserId == email)
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public static bool GetRoundVoted(RESTInterface.RoundSummaryOut round, string email)
    {
        if (round != null)
        {
            if (round.votes != null)
            {
                if (round.votes.Length > 0)
                {
                    for (int i = 0; i < round.votes.Length; i++)
                    {
                        if (round.votes[i].castUserId == email)
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public static bool GetRoundVoted(RESTInterface.RoundSummaryOut round, RESTInterface.UserSummaryOut user)
    {
        return GetRoundVoted(round, user.email);
    }

    public static bool IsFinalVote(RESTInterface.RoundOut round, int numPlayers)
    {
        int numAnswers = round.answers.Length;
        if (numAnswers < numPlayers)
            return false;

        int numVotes = TallyVotes(round);
        bool isF = numAnswers - numVotes <= 1;
        return (isF);
    }

    public static bool GetAllVotesIn(RESTInterface.RoundSummaryOut round, int numPlayers)
    {
        // INT THE CASE OF A FORCED SUMMARY, THE ROUND WILL BE NULL
        if (round == null)
            return false;

        int numAnswers = round.answers.Length;
        if (numAnswers < numPlayers)
            return false;
        int numVotes = TallyVotes(round);
        return (numVotes == numAnswers);
    }

    public static List<RESTInterface.UserOut> GetPenaltyList(RESTInterface.GameDetailOut gameDetailOut, RESTInterface.RoundOut round)
    {
        List<RESTInterface.UserOut> penaltyList = new List<RESTInterface.UserOut>();
        for (int i = 0; i < gameDetailOut.players.Length; i++)
        {
            string email = gameDetailOut.players[i].userOut.email;
            bool foundVoteCast = false;

            for (int j = 0; j < round.votes.Length; j++)
            {
                if (round.votes[j].castUserId == email)
                {
                    foundVoteCast = true;
                    break;
                }
            }
            if (!foundVoteCast)
                penaltyList.Add(gameDetailOut.players[i].userOut);
        }
        return penaltyList;
    }

    public static bool GameHasWinner(RESTInterface.GameDetailOut game)
    {
        if (game.winner != null)
            return (game.winner.userOut.email != null);
        else
            return false;
    }

    public static bool GameHasWinner(RESTInterface.GameSummaryOut game)
    {
        if (game == null)
            return false;

        if (game.winner != null)
            return (game.winner.userOut.email != null);
        else
            return false;
    }

    public static bool GetAllVotesIn(RESTInterface.RoundOut round, int numPlayers)
    {
        // INT THE CASE OF A FORCED SUMMARY, THE ROUND WILL BE NULL
        if (round == null)
            return false;

        int numAnswers = round.answers.Length;
        if (numAnswers < numPlayers)
            return false;
        int numVotes = TallyVotes(round);
        return (numVotes == numAnswers);
    }

    private static int TallyVotes(RESTInterface.RoundSummaryOut round)
    {
        if (round.votes != null)
        {
            return round.votes.Length;
        }
        else
            return 0;
    }

    public static int TallyVotes(RESTInterface.RoundOut round)
    {
        int numVotes = 0;
        if (round.votes != null)
            numVotes = round.votes.Length;
        return (numVotes);
    }

    public static string GetUsersAnswerCardID(RESTInterface.RoundOut round, RESTInterface.UserOut user)
    {
        for (int i = 0; i < round.answers.Length; i++)
        {
            if (round.answers[i].user.email.ToLower() == user.email.ToLower())
                return (round.answers[i].answerCard.id);
        }
        return null;
    }

    public static int GetNumPlayers(RESTInterface.GameSummaryOut gameSummaryOut)
    {
        return gameSummaryOut.players.Length;
    }

    public static bool GetRoomHasGame(Room room)
    {
        if (room == null)
            return false;
        if (room.games == null)
            return false;
        return (room.games.Count > 0);
    }

    public class Leader
    {
        public RESTInterface.PlayerSummaryOut player;
        public int score;
    }

    public static List<Leader> GetLeader(RESTInterface.GameSummaryOut game, Room.CurrentRoundState currentRoundState)
    {
        int leaderScore = -1000;
        List<Leader> leaderHold = new List<Leader>();
        for (int i = 0; i < game.players.Length; i++)
        {
            int numVotes = game.players[i].totalVotes;
            if (numVotes >= leaderScore)
            {
                Leader leader = new Leader();
                if (numVotes > leaderScore)
                {
                    leaderHold.Clear();
                    leaderScore = numVotes;
                }
                leader.player = game.players[i];
                leader.score = numVotes;
                leaderHold.Add(leader);
            }

#if UNITY_EDITOR
            /*
            // TESTING AVATAR, CAN PLUG IN WHATEVER YOU WANT HERE
            if( game.players[i].userOut.email.ToLower() == "dan@stovepipeinteractive.com")
            {
                leaderHold.Clear();
                Leader leader = new Leader();
                leader.player = game.players[i];
                leader.score = 5;
                leaderHold.Add(leader);
                return leaderHold;
            }
            */
#endif
        }
        return leaderHold;
    }

    public class userRoundVotesDetail
    {
        public RESTInterface.AnswerOut answer;
        public List<RESTInterface.UserOut> voters;
    }

    public class userRoundVotes
    {
        public RESTInterface.AnswerOut answer;
        public int numVotes;
        public bool defactoWinner;
    }

    public class roundVotes
    {
        public string email;
        public string cardId;
        public int numVotes;
        public bool defactoWinner;
    }

    public static List<userRoundVotesDetail> GetUserRoundVotesDetail(RESTInterface.RoundOut round)
    {
        int numEntries = round.answers.Length;
        List<userRoundVotesDetail> roundVotes = new List<userRoundVotesDetail>();
        for (int i = 0; i < numEntries; i++)
        {
            DataManager.userRoundVotesDetail rV = new DataManager.userRoundVotesDetail
            {
                answer = round.answers[i],
                voters = new List<RESTInterface.UserOut>()
            };
            roundVotes.Add(rV);
        }
        return roundVotes;
    }

    public static string GetCardIDForUserInRound(string email, RESTInterface.RoundOut round)
    {
        for (int i = 0; i < round.answers.Length; i++)
        {
            if (round.answers[i].user.email.ToLower() == email.ToLower())
                return round.answers[i].answerCard.id;
        }
        return "";
    }

    public static List<userRoundVotes> GetRoundWinner(RESTInterface.GameDetailOut game, int roundIndex)
    {
        RESTInterface.RoundOut round = game.rounds[roundIndex];
        List<roundVotes> roundVoteList = new List<roundVotes>();

        if (round.votes.Length > 0)
        {
            for (int i = 0; i < game.players.Length; i++)
            {
                string playerEmail = game.players[i].userOut.email;
                int playerVoteCount = 0;
                for (int j = 0; j < round.votes.Length; j++)
                {
                    if (playerEmail.ToLower() == round.votes[j].forUserId.ToLower())
                        playerVoteCount++;
                }
                if (playerVoteCount > 0)
                {
                    roundVotes rV = new roundVotes();
                    rV.numVotes = playerVoteCount;
                    rV.email = playerEmail;
                    rV.cardId = GetCardIDForUserInRound(playerEmail, round);
                    rV.defactoWinner = false;

                    roundVoteList.Add(rV);
                }
            }
        }
        else
        {
            roundVotes rV = new roundVotes();
            rV.numVotes = 1;
            rV.email = round.answers[0].user.email;
            rV.cardId = round.answers[0].answerCard.id;
            rV.defactoWinner = true;

            roundVoteList.Add(rV);
        }

        List<userRoundVotes> leaders = new List<userRoundVotes>();
        int leaderScore = 0;
        for (int k = 0; k < roundVoteList.Count; k++)
        {
            //Debug.Log("roundVoteList[" + k + "], cardId: " + roundVoteList[k].cardId + ", num of votes: " + roundVoteList[k].numVotes);
            bool put = false;
            if (roundVoteList[k].numVotes > leaderScore)
            {
                leaders.Clear();
                leaderScore = roundVoteList[k].numVotes;
                put = true;
            }
            else if (roundVoteList[k].numVotes == leaderScore)
                put = true;
            if (put)
            {
                userRoundVotes uRV = new userRoundVotes();
                RESTInterface.AnswerOut answer = GetAnswerFromCardId(game, roundIndex, roundVoteList[k].cardId);
                uRV.answer = answer;
                uRV.numVotes = roundVoteList[k].numVotes;
                uRV.defactoWinner = roundVoteList[k].defactoWinner;
                leaders.Add(uRV);
            }
        }
        return leaders;
    }

    public static RESTInterface.AnswerOut GetAnswerFromCardId(RESTInterface.GameDetailOut game, int roundIndex, string cardId)
    {
        RESTInterface.RoundOut round = game.rounds[roundIndex];
        for (int i = 0; i < game.players.Length; i++)
        {
            if (round.answers[i].answerCard.id == cardId)
                return round.answers[i];
        }
        return null;
    }

    public static string ReplaceQuestionCarrot(string str)
    {
        if (str.Contains("^"))
            return str.Replace("^", "____________");
        else
            return str;
    }

    public static int GetNumVotesThroughRound(RESTInterface.GameDetailOut game, RESTInterface.UserOut user)
    {
        int totalVotes = 0;
        for (int i = 0; i < game.players.Length; i++)
        {
            if (game.players[i].userOut.email.ToLower() == user.email.ToLower())
            {
                totalVotes = game.players[i].totalVotes;
                break;
            }
        }
        return totalVotes;
    }

    public static bool Get6DigitCodeValid(string value)
    {
        return value.Length == 6;
    }

    public static bool CheckEmailValid(string value)
    {
        //source:
        // https://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
        try
        {
            var addr = new System.Net.Mail.MailAddress(value);
            return addr.Address == value;
        }
        catch
        {
            return false;
        }
    }

    // SOURCE: https://stackoverflow.com/questions/5859632/regular-expression-for-password-validation
    public static bool CheckPasswordValid(string password)
    {
        //Debug.Log("Evaluate this value: " + password + "  " + Time.time );
        const int MIN_LENGTH = 8;
        const int MAX_LENGTH = 15;

        if (password == null)
            return false;

        bool meetsLengthRequirements = password.Length >= MIN_LENGTH && password.Length <= MAX_LENGTH;
        bool hasUpperCaseLetter = false;
        bool hasLowerCaseLetter = false;
        bool hasDecimalDigit = false;

        if (meetsLengthRequirements)
        {
            foreach (char c in password)
            {
                if (char.IsUpper(c)) hasUpperCaseLetter = true;
                else if (char.IsLower(c)) hasLowerCaseLetter = true;
                else if (char.IsDigit(c)) hasDecimalDigit = true;
            }
        }

        bool isValid = meetsLengthRequirements && hasUpperCaseLetter && hasLowerCaseLetter && hasDecimalDigit;
        return isValid;
    }

    public static string GetBuildInfo()
    {
        string buildInfo = "Build Version:  " + Globals.serverVersion + "." + Globals.clientVersion;
        if (Application.isEditor)
            buildInfo = "Currently developing for " + buildInfo;
        return buildInfo;
    }

    public static bool GetInternetStatus(RESTInterface.Calls call = RESTInterface.Calls.GetUser)
    {
        NetworkReachability value = Application.internetReachability;
        if (value == NetworkReachability.NotReachable)
        {
            Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.NoInternet);
        }
        return (value != NetworkReachability.NotReachable);
    }

    public static int GetTimeInSecondsEpoch()
    {
        TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
        int secondsSinceEpoch = Convert.ToInt32(t.TotalSeconds);
        return secondsSinceEpoch;
    }

    public static long GetTimeInSecondsEpochLong()
    {
        TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
        long secondsSinceEpoch = Convert.ToInt64(t.TotalSeconds);
        return secondsSinceEpoch;
    }

    public static string GetTimeUntilEnd(long startTime, RESTInterface.GameSummaryOut game, Room.CurrentRoundState state)
    {
        int amount = 0;
        if (state == Room.CurrentRoundState.Answer)
            amount = game.questionHourLimit;
        else if (state == Room.CurrentRoundState.Voting)
            amount = game.voteHourLimit;

        DateTime endTime = GetTime(startTime, amount);
        return FinalizeTime(endTime);
    }

    private static string FinalizeTime(DateTime endTime)
    {
        TimeSpan ts = endTime.Subtract(DateTime.Now);
        string countDown = string.Format("Time Remaining: {0} Hours {1} Minutes", ts.Hours, ts.Minutes);
        return countDown;
    }

    public static GameBlock.StasisStartInfo GetTimeUntilStasisStart(long startTime)
    {
        int timeZoneHours = TimeZoneInfo.Local.BaseUtcOffset.Hours;

        DateTime stasisStartTime = GetTime(startTime, GetTimePlusAmount.NoExtra);
        TimeSpan t = stasisStartTime - DateTime.Now;
        GameBlock.StasisStartInfo stasisStartInfo = new GameBlock.StasisStartInfo();
        stasisStartInfo.days = t.Days;
        stasisStartInfo.hours = t.Hours;
        stasisStartInfo.minutes = t.Minutes;

        return stasisStartInfo;
    }

    public static string GetStasisInfoText(GameBlock.StasisStartInfo stasisStartInfo)
    {
        //if ( stasisStartInfo.days > 0 )
        //    return "Next game begins in " + (stasisStartInfo.days + 1) + " days.";
        if( stasisStartInfo.hours > 0 )
            return "Next game begins in " + (stasisStartInfo.hours + 1) + " hours.";
        else if( stasisStartInfo.minutes > 30 )
            return "Next game begins in less than an hour.";
        else
            return "Next game begins in less than 30 minutes.";         
    }

    public enum GetTimePlusAmount
    {
        NoExtra,
        Answer,
        Voting
    }

    public static DateTime GetTime(long timeEpoch, int delay)
    {
        DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime date = start.AddMilliseconds(timeEpoch).ToLocalTime();
        //Debug.Log("adding hours: " + delay);
        date = date.AddHours((double)delay);
        return date.ToLocalTime();
        //return date;
    }

    public static DateTime GetTime(long timeEpoch, GetTimePlusAmount gtpa = GetTimePlusAmount.NoExtra)
    {
        DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime date = start.AddMilliseconds(timeEpoch).ToLocalTime();

        switch( gtpa )
        {
            case GetTimePlusAmount.NoExtra:
                break;
            case GetTimePlusAmount.Answer:
                date = date.AddHours((double)SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.playTurnHours));
                break;
            case GetTimePlusAmount.Voting:
                date = date.AddHours((double)SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.voteTurnHours));
                break;
        }
        date.ToLocalTime();

        return date;
    }

    public static string GetTimeSinceStart(long startTime)
    {
        string dateTime = ConvertToDateTime(startTime);
        return dateTime;
    }

    public static TimeSpan GetTimeSinceVictory(RESTInterface.GameSummaryOut gameSummary)
    {
        long victoryTimeEpoch = gameSummary.ended;
        if (victoryTimeEpoch < 1)
            victoryTimeEpoch = 0;
        DateTime victoryTime = GetTime(victoryTimeEpoch);
        DateTime nowTime = DateTime.Now.ToLocalTime();
        TimeSpan ts = nowTime.Subtract(victoryTime);
        return ts;
    }

    public static string GetEndpointArn(string deviceType)
    {
        if (Globals.tokens == null)
            return null;
        if (Globals.tokens.Length == 0)
            return null;
        for (int i = 0; i < Globals.tokens.Length; i++)
        {
            if (Globals.tokens[i].type == deviceType)
                return Globals.tokens[i].endpointArn;
        }
        return null;
    }

    public static string GetMd5Hash(string input)
    {
        MD5 md5 = MD5.Create();

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }

    public static Sprite Texture2DToSprite(Texture2D tex2D)
    {
        if (tex2D == null)
            Debug.Log("Why is tex2D null?");

        Rect rect = new Rect(0f, 0f, (float)Avatar.pixelSize, (float)Avatar.pixelSize);
        Sprite sprite = Sprite.Create(tex2D, rect, Vector2.zero);
        return sprite;
    }

    public static int GetSecondsSinceLastRefresh()
    {
        int lastTime = PlayerPrefsInterface.GetLastLobbyRefresh();
        int currTime = GetTimeInSecondsEpoch();
        int timeDiff = currTime - lastTime;
        return timeDiff;
    }

    public static bool IsPlayerInGame(string email, RESTInterface.GameSummaryOut gameSummaryOut)
    {
        if (gameSummaryOut == null)
            return false;

        for (int i = 0; i < gameSummaryOut.players.Length; i++)
        {
            if (email.ToLower() == gameSummaryOut.players[i].userOut.email.ToLower())
                return true;
        }
        return false;
    }

    public static bool IsPlayerInGame(string email, RESTInterface.GameDetailOut gameDetailOut)
    {
        if (gameDetailOut == null)
            return false;

        for (int i = 0; i < gameDetailOut.players.Length; i++)
        {
            if (email.ToLower() == gameDetailOut.players[i].userOut.email.ToLower())
                return true;
        }
        return false;
    }

    public static RESTInterface.UserOut GetVotingUserForEmail(string email, RESTInterface.RoundOut round, RESTInterface.GameDetailOut gameDetailOut)
    {
        for (int i = 0; i < gameDetailOut.players.Length; i++)
        {
            if (email == gameDetailOut.players[i].userOut.email)
                return gameDetailOut.players[i].userOut;
        }
        Debug.Log("Uh oh. This is null. Why?");
        return null;
    }

    public static List<RESTInterface.UserOut> GetVotersForEmail(string email, RESTInterface.RoundOut round, RESTInterface.GameDetailOut gameDetailOut)
    {
        List<RESTInterface.UserOut> voterList = new List<RESTInterface.UserOut>();

        for (int i = 0; i < round.votes.Length; i++)
        {
            if (round.votes[i].forUserId == email)
            {
                RESTInterface.UserOut user = GetVotingUserForEmail(round.votes[i].castUserId, round, gameDetailOut);
                voterList.Add(user);
            }
        }
        return voterList;
    }

    public static VotingPreview.GIFUrlData GetGifURLData(string submissionURL)
    {
        VotingPreview.GIFUrlData gifUrlData = new VotingPreview.GIFUrlData();
        if (submissionURL.Contains("_"))
        {
            string[] split = submissionURL.Split('_');
            gifUrlData.url = split[0];
            int topBottomInt = Convert.ToInt32(split[1]);
            gifUrlData.textPosition = (MemeMaker.textPosition)topBottomInt;
        }
        else
        {
            gifUrlData.url = submissionURL;
            gifUrlData.textPosition = MemeMaker.textPosition.Bottom;
        }
        return gifUrlData;
    }

    /*
    public static bool GetGIFExistsAtPersistentDataPath(string path)
    {
        return false;
        //return (System.IO.File.Exists(path));
    }
    */

    /*
    public static string GetGIFStoragePath(string gameId, int roundNumber)
    {
        if (string.IsNullOrEmpty(gameId))
            Debug.LogError("gameId is null or empty. That is bad.");
        return (Application.persistentDataPath + "/" + gameId + "/" + roundNumber.ToString());
    }

    public static string GetGIFFullPath(string gameId, int roundNumber, string cardId)
    {
        string path = Application.persistentDataPath + "/" + gameId + "/" + roundNumber.ToString() + "/" + cardId + gifExtension;
        //Debug.Log("GifFullPath: " + path);
        return (path);
    }
    */

    public const string gifExtension = ".gif";

    public static void StoreGifData(byte[] bytes, string gameId, int currentRound, string currentCardId)
    {
        // DO NOT STORE RIGHT NOW
        return;
        /*
        string path = GetGIFStoragePath(gameId, currentRound);

        if( !Directory.Exists(path) )
            Directory.CreateDirectory(path);
        string fullPath = path + "/" + currentCardId + gifExtension;

        File.WriteAllBytes(fullPath, bytes);
        Debug.Log(bytes.Length + " bytes written to: " + fullPath);
        */
    }

    public static byte[] GetBytesFromPath(string path)
    {
        return File.ReadAllBytes(path);
    }

    public static int GetNumOfLines(TextMeshProUGUI textElement)
    {
        return textElement.textInfo.lineCount;
    }

    public static int GetRoomIndex(string roomName, RESTInterface.GameRoomOut[] rooms)
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            if (rooms[i].name.ToLower() == roomName.ToLower())
                return i;
        }
        return -1;
    }

    public static AWSSDK.SNS.SNSManager.EndpointType GetEndpointTypeFromString(string str)
    {
        if (AWSSDK.SNS.SNSManager.EndpointType.iOS.ToString() == str)
            return AWSSDK.SNS.SNSManager.EndpointType.iOS;
        else if (AWSSDK.SNS.SNSManager.EndpointType.Android.ToString() == str)
            return AWSSDK.SNS.SNSManager.EndpointType.Android;
        else
            return AWSSDK.SNS.SNSManager.EndpointType.EditorTest;
    }

    public static string[] GetTokenArray()
    {
        List<string> stringList = new List<string>();
        stringList.Add(AWSSDK.SNS.SNSManager.allDevices_Str);
        if (Globals.tokens == null)
            return stringList.ToArray();
        else
        {
            for (int i = 0; i < Globals.tokens.Length; i++)
            {
                if (Globals.tokens[i].endpointArn != "")
                    stringList.Add(Globals.tokens[i].endpointArn);
            }
            return stringList.ToArray();
        }
    }

    public static List<Channel.EntryStatus> GetCurrentRoomEntryStatus(string roomId)
    {
        string key = PlayerPrefsInterface.RoomEntryStatusPrefix_Str + roomId;
        if (!PlayerPrefs.HasKey(key))
            return null;
        else
        {
            List<Channel.EntryStatus> entries = new List<Channel.EntryStatus>();

            string raw = PlayerPrefs.GetString(key);
            string[] playerInfo = raw.Split(' ');
            for (int i = 0; i < playerInfo.Length; i++)
            {
                string[] parts = playerInfo[i].Split('*');
                Channel.EntryStatus entry = new Channel.EntryStatus
                {
                    email = parts[0],
                    status = parts[1] == "1"
                };
                entries.Add(entry);
            }
            return entries;
        }
    }

    public static void StoreCurrentRoomEntryStatus(string roomId, List<Channel.EntryStatus> entryStatus)
    {
        string value = "";
        for (int i = 0; i < entryStatus.Count; i++)
        {
            int status = 0;
            if (entryStatus[i].status)
                status = 1;
            if (i != 0)
                value += " ";
            value += entryStatus[i].email + "*" + status.ToString();
        }

        PlayerPrefsInterface.SetRoomEntryStatus(roomId, value);
    }

    public static bool GetTooMuchChatter(Room room)
    {
        int numChatsInARowAllowed = 3;
        int startIndex = room.chatEntries.Length - numChatsInARowAllowed;
        if (startIndex < 0)
            return false;
        else
        {
            for (int i = startIndex; i < room.chatEntries.Length; i++)
            {
                if (room.chatEntries[i].user.email != Globals.email)
                    return false;
            }
            return true;
        }
    }

    public static string ConvertToDateTime(long value)
    {
        //Debug.Log("ConvertToDataTime, long input: " + value.ToString());

        long unixDate = value;
        DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime date = start.AddMilliseconds(unixDate).ToLocalTime();

        string myTime = date.ToString("hh:mm tt");
        char[] chars = myTime.ToCharArray();
        if (chars[0] == '0')
        {
            string build = "";
            for (int i = 1; i < chars.Length; i++)
                build += chars[i];
            myTime = build;
        }
        string[] split = myTime.Split(' ');
        myTime = split[0] + split[1].ToLower();

        string dayOfWeek = date.DayOfWeek.ToString();
        return dayOfWeek + " " + myTime;
    }

    public static int GetChatBadgeAmount(Room inRoom)
    {
        int numEntries = inRoom.chatEntries.Length;
        int numEntriesPostInstall = 0;
        if (numEntries > 0)
        {
            long installTime = Convert.ToInt64(PlayerPrefsInterface.GetInstallTimeEpoch());
            for (int i = 0; i < numEntries; i++)
            {
                string entryTimeStr = inRoom.chatEntries[i].date.ToString();
                entryTimeStr = entryTimeStr.Substring(0, installTime.ToString().Length);
                long entryTime = Convert.ToInt64(entryTimeStr);
                //Debug.Log("installTime: " + installTime.ToString() + "  entryTime: " + entryTime.ToString());

                if (installTime < entryTime)
                {
                    //Debug.Log(installTime.ToString().Length + " | " + entryTime.ToString().Length);
                    numEntriesPostInstall++;
                }

            }
        }
        int numSeen = PlayerPrefsInterface.GetSeenChatEntryCount(inRoom.id);
        return Mathf.Max(numEntriesPostInstall - numSeen, 0);
    }

    public static void ScrollToTop(ScrollRect scrollRect)
    {
        Vector2 currNPos = scrollRect.normalizedPosition;
        scrollRect.normalizedPosition = new Vector2(currNPos.x, 1);
    }

    public static void ScrollToBottom(ScrollRect scrollRect)
    {
        Vector2 currNPos = scrollRect.normalizedPosition;
        scrollRect.normalizedPosition = new Vector2(currNPos.x, 0);
    }

    public static void PushRectTransformToNewWSize(RectTransform rT, float width)
    {
        Vector2 currentValues = rT.sizeDelta;
        float multiplier = width / currentValues.x;
        currentValues *= multiplier;
        rT.sizeDelta = currentValues;
    }

    public static List<RESTInterface.PlayerOut> SortPlayersForTotalVotes(List<RESTInterface.PlayerOut> playerList)
    {
        IComparer<RESTInterface.PlayerOut> sorter = new sortPlayersByTotalVotesFullPlayer();
        playerList.Sort(sorter);
        return playerList;
    }

    public class sortPlayersByTotalVotesFullPlayer : IComparer<RESTInterface.PlayerOut>
    {
        public int Compare(RESTInterface.PlayerOut a, RESTInterface.PlayerOut b)
        {
            if (a.totalVotes < b.totalVotes)
                return 1;
            else if (a.totalVotes > b.totalVotes)
                return -1;
            else
                return 0;
        }
    }

    public static List<RESTInterface.PlayerSummaryOut> SortPlayersForTotalVotes(List<RESTInterface.PlayerSummaryOut> playerList)
    {
        IComparer<RESTInterface.PlayerSummaryOut> sorter = new sortPlayersByTotalVotes();
        playerList.Sort(sorter);
        return playerList;
    }

    public class sortPlayersByTotalVotes : IComparer<RESTInterface.PlayerSummaryOut>
    {
        public int Compare(RESTInterface.PlayerSummaryOut a, RESTInterface.PlayerSummaryOut b)
        {
            if (a.totalVotes < b.totalVotes)
                return 1;
            else if (a.totalVotes > b.totalVotes)
                return -1;
            else
                return 0;
        }
    }

    public static bool IsOnAdminList(string email)
    {
        if (email == null)
            return false;

        string[] adminList = new string[2]
        {
            "dan@stovepipeinteractive.com",
            "stephen.more@gmail.com"
        };
        string[] systemVarsAdminList = SystemVarsManager.instance.GetStringArray(SystemVarsManager.VariablesList.adminList);
        if (systemVarsAdminList != null)
            adminList = systemVarsAdminList;

        for (int i = 0; i < adminList.Length; i++)
        {
            if (email.ToLower() == adminList[i])
                return true;
        }
        return false;
    }

    public static bool DoTutorial()
    {
        return (PlayerPrefsInterface.GetDoTutorial());
    }

    public static bool UserIsBot(RESTInterface.UserOut user)
    {
        return user.type == robot_str;
    }

    public static bool GetAvatarInCache(string email, long avatarModified, bool useCustomAvatar)
    {
        string avatarCachePath = DataManager.GetAvatarCachePath(email, useCustomAvatar);
        bool exists = System.IO.File.Exists(avatarCachePath);

        if( exists && useCustomAvatar )
        {
            string modValue = PlayerPrefsInterface.GetLastCustomAvatarModified(email);
            long modValueLong = Convert.ToInt64(modValue) + 1;

            if (modValueLong < avatarModified)
            {
                Debug.Log("We had old cached avatar for " + email + ", we'll need to get latest avatar.");
                exists = false;
            }       
        }

        return exists;
    }

    public static string GetAvatarCachePath(string email, bool useCustomAvatar)
    {
        string emailUpdated = ReplaceAtSymbol(email);
        string path = Application.persistentDataPath + "/avatars/" + emailUpdated;

        switch ( useCustomAvatar )
        {
            case true:
                path += "/custom.png";
                break;
            case false:
                path += "/basic.png";
                break;
        }
        
        return path;
    }

    public static string ReplaceAtSymbol(string email)
    {
        string update = email.Replace("@", "_AT_");
        return update;
    }

    public static void CreateFolders(string email)
    {
        string emailUpdate = ReplaceAtSymbol(email);
        if (!Directory.Exists(Application.persistentDataPath + "/avatars"))
            Directory.CreateDirectory(Application.persistentDataPath + "/avatars");
        if (!Directory.Exists(Application.persistentDataPath + "/avatars/" + emailUpdate))
            Directory.CreateDirectory(Application.persistentDataPath + "/avatars/" + emailUpdate);
    }

    public static string GetCurrencyNameString()
    {
        return currencyName_str;
    }

    public static string GetCurrencyNameSingularString()
    {
        return currencyNameSingular_str;
    }

    public static string GetRoomNameString()
    {
        return roomName_str;
    }

    public static bool ShowAd(AdsManager.AdType type, UIStateManager.StateNames state)
    {
        if (!ShowAds(type))
            return false;
        else
        {
            return true;
            /*
            if (type == AdsManager.AdType.Native)
            {
                float percentage = 1f;
                //float lobbyPercentage = 1f;
                //float gameRoundPercentage = 1f;
                float var = UnityEngine.Random.Range(0f, 1f);
                int numBins = GameLobby.GetNumActiveBins();
                switch (state)
                {
                    case UIStateManager.StateNames.GameLobby:
                        percentage = numBins > 1 ? 0.6f : 0.75f;
                        break;
                    case UIStateManager.StateNames.GameRound:
                        percentage = numBins > 1 ? 0.2f : 0.75f;
                        break;
                }
                Debug.Log("ShowAd calc for " + state.ToString() + " var: " + var + " percentage: " + percentage);
                return var <= percentage;
            }
            else
                return true;
            */
        }
    }

    // CHECK FOR BOTH NATIVE AND INTERSTITIAL
    public static bool ShowAds(AdsManager.AdType adType)
    {
        bool memShow = false;
        bool? showAdsToMembers = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.showAdsToMembers);
        if (showAdsToMembers != null)
            memShow = (bool)showAdsToMembers;

        if ( Globals.member > 0 && !memShow )
            return false;

        if (adType == AdsManager.AdType.Native)
        {
            bool? disableNativeAds = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.disableNativeAds);
            if (disableNativeAds == null)
                return true;
            else return (bool)!disableNativeAds;
        }
        else if( adType == AdsManager.AdType.Interstitial)
        {
            bool? disableInterstitialAds = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.disableInterstitialAds);
            if (disableInterstitialAds == null)
                return true;
            else return (bool)!disableInterstitialAds;
        }
        return true;
    }

    public static int GetDaysOfRefreshToken()
    {
        int createTime = PlayerPrefsInterface.GetRefreshTokenCreationTimeEpoch();
        int now = DataManager.GetTimeInSecondsEpoch();
        int delta = now - createTime;
        //Debug.Log("now: " + now + " createTime: " + createTime);
        return Mathf.RoundToInt(Mathf.Floor((float)delta / 86400f));
    }

    public static UIStateManager.StateNames GetStateOfItem(GameObject gameObject)
    {
        UIState state = FindUIStateRecursively(gameObject);
        if (state != null)
            return state.uiState;
        else
            return UIStateManager.StateNames.NUM_OF_UISTATES;
    }

    private static UIState FindUIStateRecursively(GameObject gO)
    {
        UIState s = gO.GetComponent<UIState>();
        if (s == null)
        {
            //Debug.Log("UIState is null on: " + gO.name);
            if (gO.transform.parent != null)
            {
                Transform parent = gO.transform.parent;
                s = FindUIStateRecursively(parent.gameObject);
            }
            else
            {
                return null;
            }
        }
        return s;
    }

    public static int GetRewardAdEarnAmount()
    {
        int defaultAmount = 10;
        int? earnAmount = SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.watchAdEarnedTokens);
        return earnAmount == null ? defaultAmount : (int)earnAmount;
    }

    public static void UpdateCurrency(int delta)
    {
        RESTInterface.instance.UpdateCurrency(delta);
    }

    public static bool OfferRewardedAd()
    {
        if (!Globals.CurrencyAvailableForGame())
        {
            Globals.MemeLog(MethodBase.GetCurrentMethod(), "Do not offer rewarded ad. Currency Not Available for game.");
            return false;
        }   

        if (SystemVarsManager.instance == null)
            return false;

        bool? disableRewardAds = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.disableRewardAds);
        return disableRewardAds == null ? true : !(bool)disableRewardAds;
    }

    public static bool OfferIAPTokens()
    {
        bool? disableIAPTokens = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.disableIAPTokens);
        return disableIAPTokens == null ? true : !(bool)disableIAPTokens;
    }

    public static int GetIAPNumSKUSToShow()
    {
        int defaultNumber = 3;
        int? numToShow = SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.IAPNumSKUSToShow);
        return numToShow == null ? defaultNumber : (int)numToShow;
    }

    public static int GetIAPCurrencyAmount(int iapButtonIndex)
    {
        // FOR NOW WE'RE JUST GONNA HARDCODE TIER 1 USERS, NOT WHALES OR ANYTHING OF THAT NATURE
        // THIS GOES FOR THE SKUs BELOW AS WELL

        int[] tempAmounts = new int[4] { 50, 150, 350, 800 };
        int?[] tier1Amounts = SystemVarsManager.instance.GetIntArray(SystemVarsManager.VariablesList.tier1IAPcurrency);
        if (tier1Amounts != null)
            return (int)tier1Amounts[iapButtonIndex];
        else
            return tempAmounts[iapButtonIndex];
    }

    public static int GetIAPCurrencySKU(int iapButtonIndex)
    {
        // CURRENCY PACK - SMALL, CURRENCY PACK - MEDIUM, CURRENCY PACK - LARGE
        int[] tempSKUIndexes = new int[4] { 0, 3, 7, 13 };
        int?[] tier1IAPSKUs = SystemVarsManager.instance.GetIntArray(SystemVarsManager.VariablesList.tier1IAPSKUs);
        if (tier1IAPSKUs != null)
            return (int)tier1IAPSKUs[iapButtonIndex];
        else
            return tempSKUIndexes[iapButtonIndex];
    }

    public static int GetIAPSKUCount()
    {
        // SEE BELOW ALSO, WE'LL BASE IT ON THAT EDITOR/TEMP SETUP
        return 18;
    }

    public static string GetIAPCurrencyCost(int skuIndex)
    {
        // THIS WILL BE RETRIEVED FROM A STORE ON DEVICE

        // HERE IS A POSSIBLE SKU LAYOUT WE MAY IMPLEMENT ON THE STORE SIDE
#if UNITY_EDITOR
        string[] tempValues = new string[18]
        {
            "0.99",     // SMALL OPTION 1       0
            "1.99",     // SMALL OPTION 2       1
            "2.99",     // SMALL OPTION 3       2

            "2.99",     // MEDIUM OPTION 1      3
            "3.99",     // MEDIUM OPTION 2      4
            "4.99",     // MEDIUM OPTION 3      5
            "9.99",     // MEDIUM OPTION 4      6

            "4.99",     // LARGE OPTION 1       7
            "9.99",     // LARGE OPTION 2       8
            "19.99",    // LARGE OPTION 3       9
            "29.99",    // LARGE OPTION 4       10
            "49.99",    // LARGE OPTION 5       11
            "99.99",    // LARGE OPTION 6       12

            "9.99",     // MEGA OPTION 1        13
            "19.99",    // MEGA OPTION 2        14
            "29.99",    // MEGA OPTION 3        15
            "49.99",    // MEGA OPTION 4        16
            "99.99"     // MEGA OPTION 5        17
        };
        return "$~" + tempValues[skuIndex];
#else
        return "-";
#endif
    }

    public static bool GetUseCustomAvatar(string email, int member, long avatarModified = 0, long created = 0)
    {
        if (member > 0)
        {
            if (avatarModified > 0)
                return true;
            else
                return false;
        }
        else if (member == 0)
        {
            if (avatarModified > 0)
            {
                bool inTrialPeriod = InTrialPeriod(member, created);
                return inTrialPeriod;
            }
            else
                return false;
        }
        else
            return false;
    }

    private static IAmazonS3 s3Client;

    private static void ConfigureS3Client()
    {
        s3Client = AWSSDK.SNS.SNSManager.instance.GetS3Client();
    }

    public static void S3PostObject(string fileName = "")
    {
        if (fileName == "")
            fileName = DataManager.ReplaceAtSymbol(Globals.email);

        ConfigureS3Client();
        string S3BucketName = "quipple-avatars";

        var stream = new FileStream(Application.persistentDataPath +
        Path.DirectorySeparatorChar + "avatars" + Path.DirectorySeparatorChar + fileName + Path.DirectorySeparatorChar + "custom.png",
        FileMode.Open, FileAccess.Read, FileShare.Read);

        string outputFile = DataManager.GetMd5Hash(Globals.email);
        var request = new PutObjectRequest()
        {
            BucketName = S3BucketName,
            Key = outputFile + ".png",
            InputStream = stream,
            CannedACL = S3CannedACL.PublicRead,
        };

        try
        {
            s3Client.PutObjectAsync(request, (responseObj) => {

                if (responseObj.Exception == null)
                {
                    Debug.Log(string.Format("object {0} posted to bucket {1}", responseObj.Request.Key, responseObj.Request.BucketName));
                    AWSSDK.SNS.SNSManager.instance.AvatarPostedToS3();
                }
                else
                {
                    Debug.Log("\nException while posting the result object");
                    Debug.Log(string.Format("\n receieved error {0}", responseObj.Response.HttpStatusCode.ToString()));
                }
            });
        }
        catch (Exception ex)
        {
            Debug.Log("Unhandled Exception while posting the result object");
            Debug.Log(string.Format("\n received error {0}", ex.ToString()));

        };
    }

    public static bool UserIsMember(int memberValue)
    {
        return memberValue > 0;
    }

    public static string GetCurrentFavorites()
    {
        if (Globals.favoritesIDs == null)
            return "";
        else if (Globals.favoritesIDs.Count == 0)
            return "";
        else if (Globals.favoritesIDs.Count == 1)
            return Globals.favoritesIDs[0];
        else
            return SetCommaBasedString(Globals.favoritesIDs);
    }

    public static RESTInterface.UserIn ConfigureUserInInfo(string displayName = null, bool? allowNotifications = null, RESTInterface.TokenOut[] tokens = null, long? avatarModified = null, int? member = null, int? currency = null, string favorites = null)
    {
        RESTInterface.UserIn userInInfo = new RESTInterface.UserIn();
        userInInfo.displayName = displayName == null ? Globals.displayName : displayName;
        userInInfo.allowNotifications = allowNotifications == null ? Globals.allowNotifications : (bool)allowNotifications;
        userInInfo.tokens = tokens == null ? Globals.tokens : tokens;
        userInInfo.avatarModified = avatarModified == null ? Globals.avatarModified : (long)avatarModified;
        userInInfo.member = member == null ? Globals.member.ToString() : ((int)member).ToString();
        userInInfo.currency = currency == null ? (Globals.GetCurrentCurrency()).ToString() : ((int)currency).ToString();
        userInInfo.favorites = favorites == null ? GetCurrentFavorites() : favorites;

        Debug.Log("Lets report this displayName inbound: " + userInInfo.displayName);
        return userInInfo;
    }

    public static long GetNowTime()
    {
        long nowTime = System.DateTime.Now.Ticks;
        return nowTime;
    }

    private static bool InTrialPeriod(int member, long created)
    {
        if( member > 0 )
        {
            Globals.MemeLog(MethodBase.GetCurrentMethod(), "WE CHECKED InTrialPeriod FOR A MEMBER, WHY? FIX THIS.");
            return true;
        }

        if (member < 0)
            return false;

        DateTime nowTime = System.DateTime.Now;
        DateTime createdTime = GetTime(created);
        TimeSpan ts = nowTime - createdTime;
        int daysPassed = ts.Days;

        if( created < 1 )
            daysPassed = 21;

        int defaulttrialDays = 30;
        int? systemTrialPeriodDays = SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.trialPeriodDays);
        if (systemTrialPeriodDays == null)
            systemTrialPeriodDays = defaulttrialDays;

        Debug.Log("Checking InTrialPeriod, daysPassed: " + daysPassed + " trialPeriodDays: " + systemTrialPeriodDays);

        return daysPassed <= systemTrialPeriodDays;
    }

    public static void CheckNewUserExpiration()
    {
        if (Globals.member != 0) return;

        bool inTrialPeriod = InTrialPeriod(0, Globals.created);
        if (!inTrialPeriod)
        {
            RESTInterface.instance.SetMemberStatus(Globals.MemberNum.NonMember);
            Globals.member = (int)Globals.MemberNum.NonMember;
        }
        Debug.Log("CheckNewUserExpiration, inTrialPeriod: " + inTrialPeriod);
    }

    public static bool IsWhitelistedMember(string email = "", int member = 0)
    {
        if (member == (int)Globals.MemberNum.Member) return false;

        if (email == "")
            email = Globals.email;

        string[] members = new string[0];
        string[] memList = SystemVarsManager.instance.GetStringArray(SystemVarsManager.VariablesList.whitelistMembers);
        if (memList != null)
            members = memList;

        for(int i = 0; i < members.Length; i++ )
        {
            if (email == members[i])
            {
                Globals.onWhitelist = true;
                if (member != (int)Globals.MemberNum.Whitelisted)
                    RESTInterface.instance.SetMemberStatus(Globals.MemberNum.Whitelisted);
                return true;
            }      
        }
        if( member == (int)Globals.MemberNum.Whitelisted )
        {
            Globals.member = (int)Globals.MemberNum.NonMember;
            RESTInterface.instance.SetMemberStatus(Globals.MemberNum.NonMember);
        }        
        Globals.onWhitelist = false;
        return false;
    }

    public static string ColorizeString(string source, Color color)
    {
        string colorHex = ColorUtility.ToHtmlStringRGB(color);
        return string.Format("<color=#" + colorHex + ">{0}</color>", source);
    }

    public static int GetMyPreferredWaitTime(int hours)
    {
        bool inHours = false;
        bool? delayInHours = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.TestNewGameInHours);
        if (delayInHours != null)
        {
            if ((bool)delayInHours)
                inHours = true;
        }

        return inHours ? hours : hours / 24;
    }

    public static Channel.PlayerRoomPreferences GetCurrentRoomPreferences(Room room, string email)
    {
        for (int i = 0; i < room.roomPlayers.Count; i++)
        {
            if (email == room.roomPlayers[i].user.email)
            {
                RESTInterface.RoomPlayerOut roomPlayer = room.roomPlayers[i];
                Channel.PlayerRoomPreferences playerRoomPreferences = new Channel.PlayerRoomPreferences();

                playerRoomPreferences.hoursOrDaysBreak = GetMyPreferredWaitTime(roomPlayer.gameDelayHours);

                playerRoomPreferences.availableForNextGame = roomPlayer.availableForNextGame;
                playerRoomPreferences.seeAllNotificationActions = roomPlayer.notifyForOther;
                playerRoomPreferences.seeChatAsNotifications = roomPlayer.notifyForChat;
                return playerRoomPreferences;
            }
        }
        return null;
    }

    public static bool GetHasServerError(DownloadHandler downloadHandler)
    {
        return (downloadHandler.text.ToLower().Contains("internal server error") || downloadHandler.text.Contains("status code: 500"));
    }

    public static bool ShowTestInterstitialButton()
    {
        if (IsOnAdminList(Globals.email))
        {
            bool? testInterstitials = SystemVarsManager.instance.GetFlag(SystemVarsManager.VariablesList.TestInterstitials);
            if (testInterstitials != null)
                return (bool)testInterstitials;
            else
                return false;
        }
        else
            return false;
    }

    public static int GetRoundSwapLimit()
    {
        int? limit = SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.SwapsPerRoundLimit);
        if (limit == null)
            return 1;
        else
            return (int)limit;
    }

    public static RESTInterface.PlayerOut GetGamePlayer(RESTInterface.GameDetailOut gameDetailOut, string email)
    {
        for (int i = 0; i < gameDetailOut.players.Length; i++)
        {
            if (email == gameDetailOut.players[i].userOut.email)
                return gameDetailOut.players[i];
        }
        return null;
    }

    public static Texture2D RotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }

    public static Texture2D FlipTexture(Texture2D original)
    {
        Texture2D flip = new Texture2D(original.width, original.height);

        for (int i = 0; i < flip.width; i++)
        {
            for (int j = 0; j < flip.height; j++)
            {
                flip.SetPixel(flip.width - i - 1, j, original.GetPixel(i, j));
            }
        }
        flip.Apply();
        return flip;
    }

    public static bool GetImageIsInFavorites(string gifID)
    {
        if (Globals.favoritesIDs == null)
            return false;  
        else
            return Globals.favoritesIDs.Contains(gifID);   
    }

    public static string SetCommaBasedString(List<string> stringList)
    {
        string[] strArray = stringList.ToArray();
        return SetCommaBasedString(strArray);
    }

    public static string SetCommaBasedString(string[] stringArray)
    {
        if (stringArray == null)
            return "";
        else if (stringArray.Length == 0)
            return "";
        else
        {
            string build = "";
            for (int i = 0; i < stringArray.Length; i++)
            {
                build += stringArray[i];
                if (i < stringArray.Length - 1)
                    build += ",";
            }
            build = build.TrimStart(',');
            build = build.TrimEnd(',');
            return build;
        }
    }

    public static List<string> GetStringListFromCommaDelimited(string source)
    {
        if (source == null)
            return null;

        string[] split = source.Split(',');
        List<string> stringList = new List<string>();
        for (int i = 0; i < split.Length; i++)
            stringList.Add(split[i]);
        return stringList;
    }

    public static void AddFavorite(string gifID)
    {
        List<string> favList = new List<string>();
        if (Globals.favoritesIDs != null)
            favList = Globals.favoritesIDs;
        if( favList.Contains(gifID))
        {
            Debug.Log("Can't add to favorites, already in there: " + gifID);
            return;
        }    
        favList.Add(gifID);
        RESTInterface.instance.UpdateFavorites(favList);
    }

    public static void TestReplaceFavorites()
    {
        string testString = "13565455,8171834,8171833,18228481,16794839,16343059,9735302,14062361,9735309,6122925,15614732";
        List<string> stringList = GetStringListFromCommaDelimited(testString);
        RESTInterface.instance.UpdateFavorites(stringList);
    }

    public static void RemoveFavorite(string gifID)
    {
        List<string> favList = new List<string>();
        if (Globals.favoritesIDs != null)
            favList = Globals.favoritesIDs;
        favList.Remove(gifID);

        RESTInterface.instance.UpdateFavorites(favList);
    }

    public static int GetVictoryPointsForPlayerAmount(int playerAmount)
    {
        string key = "PointsForVictory" + playerAmount.ToString() + "Player";
        SystemVarsManager.VariablesList var = SystemVarsManager.GetVariable(key);
        int? value = SystemVarsManager.instance.GetInt(var);
        return value == null ? 7 : (int)value;
    }

    public static bool NewestGameWasDeleted(Room room, RESTInterface.GameSummaryOut game)
    {
        if (game == null)
            return false;

        int gameCount = room.fullGames.Count;

        int gameIndex = -1;
        //Debug.Log("fullGames.Count: " + gameCount);
        for (int i = 0; i < gameCount; i++ )
        {
            bool d = room.fullGames[i].deleted > 1;
            //Debug.Log(i + ": " + room.fullGames[i].started + " finished: " + room.fullGames[i].ended + " deleted: " + d);
            if (room.fullGames[i] == game)
                gameIndex = i;
        }
        if( gameIndex == -1 )
            return false;
        //Debug.Log("gameIndex: " + gameIndex);

        if (gameIndex != (gameCount - 1))
        {
            RESTInterface.GameSummaryOut lastGameOnStack = room.fullGames[gameCount - 1];
            return lastGameOnStack.deleted > 1;
        }
        else
            return false;
    }


    public class sortGamesByStartTime : IComparer<RESTInterface.GameSummaryOut>
    {
        public int Compare(RESTInterface.GameSummaryOut a, RESTInterface.GameSummaryOut b)
        {
            if (a.started > b.started)
                return 1;
            else if (a.started < b.started)
                return -1;
            else
                return 0;
        }
    }

    public static List<RESTInterface.GameSummaryOut> SortGamesByStarted(List<RESTInterface.GameSummaryOut> gameList)
    {
        IComparer<RESTInterface.GameSummaryOut> sorter = new sortGamesByStartTime();
        gameList.Sort(sorter);
        return gameList;
    }

    public static string ReportThisTimespan()
    {
        long finishTime = 1606865517195;
        long nextStart = 1606865517630;

        DateTime finishT = GetTime(finishTime);
        DateTime nextStartT = GetTime(nextStart);
        TimeSpan ts = nextStartT - finishT;
        return ts.ToString();
    }

    public static string GenerateUniqueID()
    {
        return Guid.NewGuid().ToString("N");
    }

    public static void CopyToClipboard(string str)
    {
        GUIUtility.systemCopyBuffer = str;
    }

    public static bool VerifyBuildCapability()
    {
        bool editorPreview = PopupOutOfDateBuild.instance.GetEditorPreview();
        if (!Application.isEditor)
            editorPreview = false;

        if (editorPreview)
            return false;

        int? minimumBuildNum = SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.MinimumBuildNum);
        if (minimumBuildNum != null)
            return (Globals.clientVersion >= (int)minimumBuildNum);
        return true;
    }

    public static bool GetIsNeutered(Room room, RESTInterface.UserSummaryOut user)
    {
        RESTInterface.RoomPlayerOut roomPlayer = GetRoomPlayerOut(room, user);
        bool isNeutered = GetIsNeutered(roomPlayer);
        return isNeutered;
    }

    private static bool GetIsNeutered(RESTInterface.RoomPlayerOut roomPlayer)
    {
        return roomPlayer.neutered;
    }

    public static bool GetIsNeutered(Room room, RESTInterface.UserOut user)
    {
        RESTInterface.RoomPlayerOut roomPlayer = GetRoomPlayerOut(room, user);
        if (roomPlayer == null)
            return false;

        bool isNeutered = GetIsNeutered(roomPlayer);
        return isNeutered;
    }

    public static RESTInterface.RoomPlayerOut GetRoomPlayerOut(Room room, RESTInterface.UserSummaryOut user)
    {
        return GetRoomPlayerOut(room, user.email);
    }

    public static RESTInterface.RoomPlayerOut GetRoomPlayerOut(Room room, RESTInterface.UserOut user)
    {
        return GetRoomPlayerOut(room, user.email);
    }

    private static RESTInterface.RoomPlayerOut GetRoomPlayerOut(Room room, string email)
    {
        for( int i = 0; i < room.roomPlayers.Count; i++ )
        {
            if (room.roomPlayers[i].user.email == email)
                return room.roomPlayers[i];
        }
        return null;
    }

    public static bool ShowToGroupSettingsBlirb()
    {
        int leaderboardVisits = PlayerPrefsInterface.GetLeaderboardVisits();
        Debug.Log("leaderboardVisits: " + leaderboardVisits + "  " + Time.time);
        return leaderboardVisits % 5 == 0;
    }
}