﻿// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// ALL COMMUNICATION TO LOCAL STORAGE FLOWS THROUGH HERE.
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

using System;
using UnityEngine;
using System.Reflection;

public static class PlayerPrefsInterface
{

    private const string LastUserIdLoggedIn_Str = "LastUserIdLoggedIn";
    private const string LoginID_Str = "LoginID";
    private const string DisplayName_Str = "DisplayName";
    private const string LoginStatus_Str = "LoginStatus";
    private const string DebugBypassEmail_Str = "DebugBypassEmail";
    private const string ChatPrefix_Str = "Chat_";
    private const string TimestampStoragePrefix_Str = "TSS_";
    private const string RoundSummarySeenPrefix_Str = "RSS_";
    public const string RoomEntryStatusPrefix_Str = "RES_";
    public const string PokePrefix_Str = "POKE*";
    private const string TopicPrefix_Str = "Topic_";
    private const string LobbyRefresh_Str = "LastLobbyRefresh";
    private const string BuildNumber_Str = "BuildNumber";
    private const string LastEndpoint_Str = "LastDeviceEndpoint";
    private const string SFX_Str = "SFX";
    private const string Music_Str = "Music";
    private const string DebugInfo_Str = "DebugInfo";
    private const string SeenTutorial_Str = "SeenTutorial";
    private const string RandomSlots_Str = "RSLOTS_";
    private const string mmInsultTextIndex_Str = "InsultIndex";
    private const string dragThreshold_Str = "DragThreshold";
    private const string installTime_Str = "InstallTime";
    private const string refreshTokenTime_Str = "RefreshTokenTime";
    private const string lastAvatarColor_Str = "LastAvatarColor";
    private const string customAvatarTimestampPrefix_Str = "CAV_";
    private const string leaderboardVisits_Str = "LBVisits";
        
    public static string GetLastUserIdLoggedIn()
    {
        if (PlayerPrefs.HasKey(LastUserIdLoggedIn_Str))
            return PlayerPrefs.GetString(LastUserIdLoggedIn_Str);
        else
        {
            PlayerPrefs.SetString(LastUserIdLoggedIn_Str, "");
            PlayerPrefs.Save();
            return "";
        }
    }

    public static Globals.LoginStatusOptions GetLoginStatus()
    {
        if (PlayerPrefs.HasKey(LoginStatus_Str))
        {
            string str = PlayerPrefs.GetString(LoginStatus_Str);
            for (int i = 0; i < (int)Globals.LoginStatusOptions.NUM_OF_LOGINSTATUSOPTIONS; i++)
            {
                if (((Globals.LoginStatusOptions)i).ToString() == str)
                    return (Globals.LoginStatusOptions)i;
            }
            return Globals.LoginStatusOptions.NotLoggedIn;
        }
        else
            return Globals.LoginStatusOptions.NotLoggedIn;
    }

    public static void SetLoginStatus(Globals.LoginStatusOptions method)
    {
        PlayerPrefs.SetString(LoginStatus_Str, method.ToString());
        PlayerPrefs.Save();
    }

    public static string GetDisplayName()
    {
        if (PlayerPrefs.HasKey(DisplayName_Str))
            return PlayerPrefs.GetString(DisplayName_Str);
        else
            return "";
    }

    public static void SetDisplayName(string str)
    {
        PlayerPrefs.SetString(DisplayName_Str, str);
        PlayerPrefs.Save();
    }

    public static void SetDebugBypassEmail(string str)
    {
        PlayerPrefs.SetString(DebugBypassEmail_Str, str);
        PlayerPrefs.Save();
    }

    public static string GetDebugBypassEmail()
    {
        if (PlayerPrefs.HasKey(DebugBypassEmail_Str))
            return PlayerPrefs.GetString(DebugBypassEmail_Str);
        else
            return "";
    }

    private static string GetChatKey(string roomId, string user)
    {
        return (ChatPrefix_Str + roomId + "_" + user);
    }

    public static void SetSeenChatEntryCount(string roomId, int count)
    {
        //Debug.Log("Set seen chat: " + roomId + ", count: " + count);
        string key = GetChatKey(roomId, Globals.email);
        PlayerPrefs.SetInt(key, count);
        PlayerPrefs.Save();
    }

    public static int GetSeenChatEntryCount(string roomId)
    {
        string key = GetChatKey(roomId, Globals.email);
        if (PlayerPrefs.HasKey(key))
            return PlayerPrefs.GetInt(key);
        else
            return 0;
    }

    public static string GetRoundSummarySeenKey(string gameId)
    {
        string currentEmail = Globals.email;
        return (RoundSummarySeenPrefix_Str + currentEmail + "_" + gameId);
    }

    public static int GetLastRoundSummarySeen(RESTInterface.GameSummaryOut gameSummaryOut)
    {
        int roundNum = 0;
        string key = GetRoundSummarySeenKey(gameSummaryOut.id);
        if (PlayerPrefs.HasKey(key))
            roundNum = PlayerPrefs.GetInt(key);
        return roundNum;
    }

    private static void SetLastRoundSummarySeen(string gameId, int roundNumberSeen)
    {
        string key = GetRoundSummarySeenKey(gameId);
        PlayerPrefs.SetInt(key, roundNumberSeen);
        PlayerPrefs.Save();
    }

    public static void SetLastRoundSummarySeen(RESTInterface.GameDetailOut gameDetailOut, int roundNumberSeen) // Actual Round # starting with 1, not an index value starting with zero
    {
        if( Globals.saveRoundSummarySeenInfo )
            SetLastRoundSummarySeen(gameDetailOut.id, roundNumberSeen);
    }

    public static string GetStoredTimestampKey(RESTInterface.GameSummaryOut gameSummaryOut)
    {
        return (TimestampStoragePrefix_Str + gameSummaryOut.id + "_" + Globals.email);
    }

    public static GameBlock.TimestampStorage GetStoredTimestamp(RESTInterface.GameSummaryOut gameSummaryOut)
    {
        string key = GetStoredTimestampKey(gameSummaryOut);
        if (PlayerPrefs.HasKey(key))
        {
            string value = PlayerPrefs.GetString(key);
            string[] valueSplit = value.Split('_');
            GameBlock.TimestampStorage storage = new GameBlock.TimestampStorage();
            storage.timestamp = valueSplit[0];
            storage.currentRoundState_Str = valueSplit[1];
            return storage;
        }
        else
            return null;
    }

    public static void SaveStorageUpdate(RESTInterface.GameSummaryOut gameSummaryOut, GameBlock.TimestampStorage storage)
    {
        string key = GetStoredTimestampKey(gameSummaryOut);
        PlayerPrefs.SetString(key, storage.timestamp + "_" + storage.currentRoundState_Str);
        PlayerPrefs.Save();
    }

    private static string GetTopicKey(string topicArn)
    {
        return (TopicPrefix_Str + topicArn);
    }

    public static bool GetTopicSubscriptionStatus(string topicArn)
    {
        string key = GetTopicKey(topicArn);
        if (PlayerPrefs.HasKey(key))
            return (PlayerPrefs.GetInt(key) == 1);
        else
            return false;
    }

    public static int GetLastLobbyRefresh()
    {
        if (PlayerPrefs.HasKey(LobbyRefresh_Str))
            return PlayerPrefs.GetInt(LobbyRefresh_Str);
        else
            return 0;
    }

    public static void SetLastLobbyRefresh()
    {
        int timeEpoch = DataManager.GetTimeInSecondsEpoch();
        PlayerPrefs.SetInt(LobbyRefresh_Str, timeEpoch);
        PlayerPrefs.Save();
    }

    public static void SetRoomEntryStatus(string roomId, string value)
    {
        string key = RoomEntryStatusPrefix_Str + roomId;
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }

    public static bool GetPokedThemThisTurn(string gameId, string email, int roundNumber)
    {
        string pokeKey = GetPokeKey(gameId, email);
        if (PlayerPrefs.HasKey(pokeKey))
        {
            int lastRoundPokeMatch = PlayerPrefs.GetInt(pokeKey);
            if (lastRoundPokeMatch > roundNumber)
                Debug.Log("THIS IS BAD, IT SHOULD NEVER BE GREATER.");
            return lastRoundPokeMatch == roundNumber;
        }
        else
            return false;
    }

    public static void SetLastPoke(string gameId, string email, int roundNumber)
    {
        string pokeKey = GetPokeKey(gameId, email);
        PlayerPrefs.SetInt(pokeKey, roundNumber);
        PlayerPrefs.Save();
    }

    private static string GetPokeKey(string gameId, string email)
    {
        string key = PokePrefix_Str + Globals.email + "*" + email + "*" + gameId;
        //Debug.Log("Poke Key: " + key);
        return key;
    }

    public static void SetSFX(bool value)
    {
        PlayerPrefs.SetInt(SFX_Str, value == true ? 1 : 0);
        PlayerPrefs.Save();
    }

    public static void SetMusic(bool value)
    {
        PlayerPrefs.SetInt(Music_Str, value == true ? 1 : 0);
        PlayerPrefs.Save();
    }

    public static bool GetSFX()
    {
        int val = 1;
        if (PlayerPrefs.HasKey(SFX_Str))
            val = PlayerPrefs.GetInt(SFX_Str);
        else
            SetSFX(true);
        return val == 1;
    }

    public static bool GetMusic()
    {
        int val = 1;
        if (PlayerPrefs.HasKey(Music_Str))
            val = PlayerPrefs.GetInt(Music_Str);
        else
            SetMusic(true);
        return val == 1;
    }

    public static bool GetDebugInfo(bool defaultValue = false)
    {
        bool val = defaultValue;
        val = GetBoolValue(DebugInfo_Str, defaultValue);
        return val;
    }

    public static void SetDebugInfo(bool value)
    {
        SetBoolValue(value, DebugInfo_Str);
    }

    private static bool GetBoolValue(string key, bool defaultValue)
    {
        int val = defaultValue ? 1 : 0;
        if (PlayerPrefs.HasKey(key))
            val = PlayerPrefs.GetInt(key);
        else
            SetDebugInfo(defaultValue);
        return val == 1;
    }

    private static void SetBoolValue(bool value, string key)
    {
        PlayerPrefs.SetInt(key, value == true ? 1 : 0);
        PlayerPrefs.Save();
    }

    public static bool GetDoTutorial()
    {
        if (PlayerPrefs.HasKey(SeenTutorial_Str))
        {
            int value = PlayerPrefs.GetInt(SeenTutorial_Str);
            return value != 1;
        }
        else
            return true;
    }

    public static void SetDidTutorial(bool didIt = true)
    {
        PlayerPrefs.SetInt(SeenTutorial_Str, didIt ? 1 : 0);
        PlayerPrefs.Save();
    }

    public static void SetRandomSlots(Room room, int amount)
    {
        string key = GetRandomSlotsKey(room);
        PlayerPrefs.SetInt(key, amount);
        PlayerPrefs.Save();
    }

    public static int GetRandomSlots(Room room)
    {
        string key = GetRandomSlotsKey(room);
        if (PlayerPrefs.HasKey(key))
        {
            if (key.Contains(Globals.displayName))
            {
                int value = PlayerPrefs.GetInt(key);
                return value;
            }
            else
                return 0;
        }    
        else
        {
            return 0;
        }    
    }

    private static string GetRandomSlotsKey(Room room)
    {
        return RandomSlots_Str + room.displayName + "_" + Globals.displayName;
    }

    public static int GetInsultIndex()
    {
        string key = mmInsultTextIndex_Str;
        int current = 0;
        int newValue = 0;
        if (PlayerPrefs.HasKey(key))
        {
            current = PlayerPrefs.GetInt(key);
            newValue = current == 0 ? 1 : 0;
        }
        PlayerPrefs.SetInt(key, newValue);
        PlayerPrefs.Save();
        return current;
    }

    public static int GetDragThreshold()
    {
        if (PlayerPrefs.HasKey(dragThreshold_Str))
            return PlayerPrefs.GetInt(dragThreshold_Str);
        else
            return Globals.dragThresholdDefault;
    }

    public static int SetDragThreshold(bool valueGoUp)
    {
        int currentValue = GetDragThreshold();
        int moveAmount = valueGoUp ? 1 : -1;
        int newValue = currentValue + moveAmount;
        PlayerPrefs.SetInt(dragThreshold_Str, newValue);
        PlayerPrefs.Save();
        return newValue;
    }

    public static long StoreInstallationTime()
    {
        long epoch = DataManager.GetTimeInSecondsEpochLong();
        PlayerPrefs.SetString(GetInstallTimeKey(), epoch.ToString());
        PlayerPrefs.Save();
        return epoch;
    }

    private static string GetInstallTimeKey()
    {
        return installTime_Str;
    }

    public static long GetInstallTimeEpoch()
    {
        if (PlayerPrefs.HasKey(GetInstallTimeKey()))
        {
            long epoch = Convert.ToInt64( PlayerPrefs.GetString(GetInstallTimeKey() ) );
            return epoch;
        }
        else
            return StoreInstallationTime();
    }

    private static string GetRefreshTokenTimeKey()
    {
        return refreshTokenTime_Str;
    }

    public static int StoreRefreshTokenCreationTime()
    {
        int epoch = DataManager.GetTimeInSecondsEpoch();
        PlayerPrefs.SetInt(GetRefreshTokenTimeKey(), epoch);
        PlayerPrefs.Save();
        return epoch;
    }

    public static int GetRefreshTokenCreationTimeEpoch()
    {
        if (PlayerPrefs.HasKey(GetRefreshTokenTimeKey()))
        {
            int epoch = (int)PlayerPrefs.GetInt(GetRefreshTokenTimeKey());
            return epoch;
        }
        else
            return StoreRefreshTokenCreationTime();
    }

    public static int GetLastAvatarColor()
    {
        if (PlayerPrefs.HasKey(lastAvatarColor_Str))
            return PlayerPrefs.GetInt(lastAvatarColor_Str);
        else
            return 0;
    }

    public static void SetLastAvatarColor(int index)
    {
        PlayerPrefs.SetInt(lastAvatarColor_Str, index);
        PlayerPrefs.Save();
    }

    public static string GetLastCustomAvatarModified(string email)
    {
        string key = GetCAVKey(email);
        if (PlayerPrefs.HasKey(key))
            return PlayerPrefs.GetString(key);
        else
            return "0";
    }

    private static string GetCAVKey(string email)
    {
        return customAvatarTimestampPrefix_Str + DataManager.ReplaceAtSymbol(email);
    }

    public static void StoreAvatarModifiedStamp(string email, long avatarModified)
    {
        string key = GetCAVKey(email);
        string value = avatarModified.ToString();
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }

    public static string GetLoginID()
    {
        if (PlayerPrefs.HasKey(LoginID_Str))
            return (PlayerPrefs.GetString(LoginID_Str));
        else
            return CloudManager.defaultLogin;
    }

    public static void SetLoginID(string id)
    {
        PlayerPrefs.SetString(LoginID_Str, id);
        PlayerPrefs.Save();
    }

    public static int GetLeaderboardVisits()
    {
        IncrementLeaderboardVisits();
        return (PlayerPrefs.GetInt(leaderboardVisits_Str));    
    }

    private static void IncrementLeaderboardVisits()
    {
        int currentValue = 0;
        if (PlayerPrefs.HasKey(leaderboardVisits_Str))
            currentValue = PlayerPrefs.GetInt(leaderboardVisits_Str);
        else
            currentValue = -1;

        currentValue++;
        PlayerPrefs.SetInt(leaderboardVisits_Str, currentValue);
    }
}
