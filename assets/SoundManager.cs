﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	// ADD ELEMENT NAMES TO ENUM HERE
	public enum SoundElementNames
	{
		ActionComplete1,		// 0
		ActionComplete2,		// 1
		RefreshUpdate1,
		DrumRoll,
		CymbalHit,
        CheckMarkOn,
        CheckMarkOff,
        BasicForward,
        SpecialSelect,
        BackClick,
        PictureSearch_NO,		// 10
        PictureSearch_YES,
        ChatBubble,
        PostChatEntry,
        FartSound,
        FartGuyHorn,
        PartyHorn,
        CashReg,
		ScrollBubble			// 18
	}
	//:::::::::::::::::::::::::::::::

	public static SoundManager instance;
	private AudioSource source;
	public soundElement[] elements;

	[System.Serializable]
	public class soundElement
	{
		public SoundElementNames name;
		public AudioClip clip;
		public float volume = 1.0f;
		public bool loop = false;
	}

	void Awake () {
		instance = this;
		source = this.GetComponent<AudioSource>();
	}

	public void Play (SoundElementNames name, float delay = 0f)
	{
        if (!Globals.SFX)
            return;

		bool notFound = true;
		for (int i = 0; i < elements.Length; i++)
		{
			if (elements [i].name == name)
			{
				notFound = false;
				//source.clip = elements[i].clip;
				//source.volume = elements[i].volume;
				//source.loop = elements[i].loop;
				StartCoroutine(PlayRoutine(elements[i], delay));
				break;
			}
		}
		if( notFound )
			Debug.LogWarning("No SFX found in manager for: " + name.ToString() );
	}

	private IEnumerator PlayRoutine(soundElement element, float delay)
	{
		yield return new WaitForSeconds(delay);

		source.Stop();

		source.clip = element.clip;
		source.volume = element.volume;
		source.loop = element.loop;

		if( !source.isPlaying )
			source.Play();
	}

}
