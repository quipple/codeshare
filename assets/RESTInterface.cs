﻿// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// ALL CUSTOM API COMMUNCATION TO THE SERVER SIDE DATABASE FLOWS THROUGH HERE.
// IF IT HAS TO DO WITH GAME STATE SUBMISSION OR RETRIEVAL, IT IS IN HERE.
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Reflection;

public class RESTInterface : MonoBehaviour {

    public enum Calls
    {
        GetUser,
        PutUser,
        GetGameRooms,
        GetGameRoom,
        GetGameDetail,
        PlayCard,
        VoteCard,
        PostChatToGameRoom,
        RefreshChat,
        GetGameDetailForSummary,
        PreparingForVoting,
        TallyingResults,
        CreatingGame,
        Generic,
        DeleteGame,
        DeleteChannel,
        SimulateCardSubmission,      // FOR TUTORIAL
        CreateRoom,
        RefreshGame,
        TradeInCard,
        GetSystemVariables,
        PostSystemVariable
    }
    public enum CallTrigger
    {
        Start,
        End
    }
	public static RESTInterface instance;

    public delegate void FullStopCoroutinesDelegate();
    public static event FullStopCoroutinesDelegate FullStopCoroutinesEvent;

    public delegate void CallLogDelegate(Calls call, CallTrigger trigger);
    public static event CallLogDelegate CallLog;

	public delegate void SNSRegisterDeviceDelegate();
	public static event SNSRegisterDeviceDelegate SNSRegisterDevice;

	public delegate void LoginFailedDelegate();
	public static event LoginFailedDelegate LoginFailedEvent;

	public delegate void DispatchRoomsDelegate(GameRoomOut[] rooms, string newRoomName);
	public static event DispatchRoomsDelegate DispatchRooms;

	public delegate void ChatPostedDelegate(string roomName);
	public static event ChatPostedDelegate ChatPosted;

	public delegate void GameAddedToRoomDelegate();
	public static event GameAddedToRoomDelegate GameAddedToRoom;

	public delegate void PushDisplayNameDelegate(string displayName);
	public static event PushDisplayNameDelegate PushDisplayName;

	public delegate void MainMenuAutoAuthDelegate();
	public static event MainMenuAutoAuthDelegate MainMenuAutoAuth;

	public delegate void RetrievedGameDetailDelegate(RESTInterface.GameDetailOut gameDetailOut, bool toGameRound, Room room, UIStateManager.StateNames currentState);
	public static event RetrievedGameDetailDelegate RetrievedGameDetail;

    public delegate void GameRoomJoinDelegate(string gameRoomId, Room room);
    public static event GameRoomJoinDelegate GameRoomJoin;

    public delegate void VoteCardCompleteDelegate();
    public static event VoteCardCompleteDelegate VoteCardComplete;

    public delegate void BroadcastFinalVoteSummaryInfoDelegate(RoundSummary.SummaryState summaryState);
    public static event BroadcastFinalVoteSummaryInfoDelegate BroadcastFinalVoteSummaryInfo;

    public delegate void BroadcastRoomRefreshDelegate(GameRoomOut room, bool fromRefreshClick);
    public static event BroadcastRoomRefreshDelegate BroadcastGetGameRoom;

    public delegate void PlayCardSuccessDelegate(string gameId);
    public static event PlayCardSuccessDelegate PlayCardSuccess;

    public delegate void TradeInCardSuccessDelegate(GameDetailOut gameDetailOut);
    public static event TradeInCardSuccessDelegate TradeInCardSuccess;

    public delegate void VoteCardSuccessDelegate(string gameId);
    public static event VoteCardSuccessDelegate VoteCardSuccess;

    public delegate void NewRoomCreatedDelegate(string displayName, string code);
    public static event NewRoomCreatedDelegate NewRoomCreated;

    public delegate void AvatarModifyCompleteDelegate();
    public static event AvatarModifyCompleteDelegate AvatarModifyComplete;

    public delegate void BroadcastSystemVariablesDelegate(Variable[] vars);
    public static event BroadcastSystemVariablesDelegate BroadcastSystemVariables;

    public delegate void RoomSettingsUpdateSuccessDelegate(Room room, RoomPlayerIn roomPlayerIn);
    public static event RoomSettingsUpdateSuccessDelegate RoomSettingsUpdateSuccess;

    public delegate void FavoritesUpdateDelegate();
    public static event FavoritesUpdateDelegate FavoritesUpdate;

    public delegate void DBUserCreatedDelegate(bool isEditor);
    public static event DBUserCreatedDelegate DBUserCreatedEvent;

    Coroutine GetGameRoomsCR;
    Coroutine GetGameRoomCR;


    private void FullStopCoroutines()
    {
        if (GetGameRoomsCR != null)
            StopCoroutine(GetGameRoomsCR);
        if (GetGameRoomCR != null)
            StopCoroutine(GetGameRoomCR);
    }

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // URL INFO
    public const string userSuffix = "user";
    public const string usersSuffix = "users";
    public const string systemSuffix = "system";
	public const string usersGetSuffix = "/me";
    public const string variableSuffix = "/variable";
    public const string variablesSuffix = "/variables";
	public const string gameroomsSuffix = "gamerooms";
	public const string chatSuffix = "chat";
	public const string gameSuffix = "game";
	public const string allSuffix = "/all";
    public const string joinedSuffix = "/joined";
	//public const string dealSuffix = "/deal";
	public const string startRoundSuffix = "/startRound";
    public const string tradeInSuffix = "/tradeIn";
    public const string leaveSuffix = "/leave";
    public const string gameRoomPlayersSuffix = "gameroomplayers";
    public const string settingsSuffix = "/settings";
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    [SerializeField] private Texture2D noConnectionAvatarTexture;

    private void CallComplete()
    {
        //Debug.Log("Call Complete. " + Time.time);
        callActive = false;
        activeRefresh = false;
        CallLog?.Invoke(Calls.GetGameRooms, CallTrigger.End);
    }

    private void InitiateTimeFail()
    {
        lastCallTimeout = true;
        Debug.Log("Initiate Fail. " + Time.time);
        FullStopCoroutinesEvent?.Invoke();
        FullStopCoroutines();

        if (!activeRefresh)
            Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.ServerError);
        else
            Debug.Log("Suppress popup due to activeRefresh.");

        CallComplete();
        LoadingCircle.instance.ClearOpenJobs();
    }

    private float GetFailThreshold()
    {
        float defaultValue = 6f;
        if( Globals.loggedIn != Globals.LoginStatusOptions.NotLoggedIn )
        {
            if( SystemVarsManager.instance != null )
            {
                int? threshold = SystemVarsManager.instance.GetInt(SystemVarsManager.VariablesList.CallTimeoutSecondsThreshold);
                if( threshold != null )
                    return (float)threshold;
            }
        }
        return defaultValue;
    }

    void IWRCreateListener(InternalWebRequest.Type type)
    {
        //Debug.Log("IWRCreateListener Pickup. " + Time.time);
        lastCallTimeout = false;
        callTimer = Time.time;
        callActive = true;
    }

    float callTimer = 0f;

    bool _callActive = false;
    bool callActive
    {
        get
        {
            return _callActive;
        }
        set
        {
            _callActive = value;
            //Debug.Log("Call Active: " + value + "  " + Time.time);
        }
    }


    float timeSpan = 0f;
    bool lastCallTimeout = false;
    void Update()
    {
        if( callActive )
        {
            timeSpan = Time.time - callTimer;
            if (timeSpan > GetFailThreshold() && !lastCallTimeout)
                InitiateTimeFail();
        }
    }

	void Awake ()
	{
        instance = this.gameObject.GetComponent<RESTInterface>();
        DontDestroyOnLoad(this);

        InternalWebRequest.CreateEvent += IWRCreateListener;
    }

    void OnDestroy()
    {
        Globals.MemeLog(MethodBase.GetCurrentMethod(), ":::: RESTInterface was destroyed. ::::");
    }

    public void RegisterDeviceIOS()
    {
        SNSRegisterDevice?.Invoke();
    }

    public IEnumerator GetGameRoom(string roomName, bool fromChatRefresh = false, bool fromPullRefresh = false)
    {
        activeRefresh = fromPullRefresh;
        using (UnityWebRequest www = InternalWebRequest.createGet(gameroomsSuffix + "/" + roomName))
        {
            if (DataManager.GetCoreFlow() != DataManager.CoreFlow.FinalVoteOfRound)
            {
                if (fromChatRefresh)
                    CallLog?.Invoke(Calls.RefreshChat, CallTrigger.Start);
                yield return www.SendWebRequest();
                CallComplete();

                if (CommStatus.currentCall == Calls.RefreshChat)
                    CallLog?.Invoke(Calls.RefreshChat, CallTrigger.End);
                yield return new WaitForSeconds(0.3f);
            }
            else
            {
                yield return www.SendWebRequest();
                CallComplete();
            }

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "Get Game Room", true);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                GameRoomOut gameRoomOut = JsonUtility.FromJson<GameRoomOut>(www.downloadHandler.text);

                if (gameRoomOut == null)
                {
                    Globals.MemeLog(MethodBase.GetCurrentMethod(), "room is null from GetGameRoom. That is bad.");
                    CallLog?.Invoke(Calls.RefreshGame, CallTrigger.End);
                    yield return new WaitForSeconds(0.5f);
                    Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.ServerError);
                    LoadingCircle.instance.Toggle(false, "Get Game Room. gameRoomOut Is null.");
                }

                if (DataManager.GetCoreFlow() != DataManager.CoreFlow.FinalVoteOfRound || UIStateManager.instance.GetCurrentState() == UIStateManager.StateNames.User)
                    BroadcastGetGameRoom?.Invoke(gameRoomOut, fromChatRefresh);
            }
        }
    }

    bool activeRefresh = false;
	public IEnumerator GetGameRooms (string newRoomName = null, bool fromClickRefresh = false )
	{
        activeRefresh = fromClickRefresh;
        using (UnityWebRequest www = InternalWebRequest.createGet(gameroomsSuffix + joinedSuffix))
		{
            CallLog?.Invoke(Calls.GetGameRooms, CallTrigger.Start);
            yield return www.SendWebRequest ();
            CallComplete();
            yield return new WaitForSeconds(0.8f);
            CallLog?.Invoke(Calls.GetGameRooms, CallTrigger.End);
            yield return new WaitForSeconds(0.3f);

            if (www.isNetworkError )
			{
				Debug.Log (www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "GetGameRooms", true);
            } else
			{
                if (!lastCallTimeout)
                {
                    bool serverError = DataManager.GetHasServerError(www.downloadHandler);
                    if (!serverError)
                    {
                        Globals.userLastGetGamesWith = Globals.email;
                        GameRoomOut[] rooms = new GameRoomOut[0];

                        if (www.downloadHandler.text != "[]")
                        {
                            rooms = JsonHelper.getJsonArray<GameRoomOut>(www.downloadHandler.text);
                            //Debug.Log(www.downloadHandler.text);
                            ReportRooms(rooms, Globals.keyAccess, newRoomName, Globals.loggedIn);
                        }

                        if (newRoomJoinName != null)
                        {
                            int index = DataManager.GetRoomIndex(newRoomJoinName, rooms);
                            string roomId = rooms[index].id;

                            if (roomId != null)
                            {
                                Room room = GetRoomOut(rooms[index]);
                                GameRoomJoin?.Invoke(roomId, room);
                                newRoomJoinName = null;
                            }
                        }
                        DispatchRooms?.Invoke(rooms, newRoomName);
                    }
                    else
                        Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.ServerError);
                }
			}
			LoadingCircle.instance.Toggle(false, "GetGameRooms");
        }
	}

    private Room GetRoomOut(GameRoomOut gameRoomOut)
    {
        Room room = new Room
        {
            displayName = gameRoomOut.displayName,
            name = gameRoomOut.name,
            id = gameRoomOut.id,
            pin = gameRoomOut.code
        };
        return room;
    }

    // EXIT ROOM
    public IEnumerator DeleteSelfFromRoom(Room room)
    {
        using (UnityWebRequest www = InternalWebRequest.createDelete(gameroomsSuffix + "/" + room.name + leaveSuffix))
        {
            yield return www.SendWebRequest();
            CallComplete();

            if (www.isNetworkError)
            {
                Debug.LogError(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "DeleteSelfFromRoom", true);
            }
            else
            {
                if (www.downloadHandler.text.ToLower().Contains("unauthorized"))
                {
                    Debug.LogError("Can't Leave, Unauthorized.");
                    LoadingCircle.instance.Toggle(false, "DeleteSelfFromRoom, unauthorized");
                }
                else
                {
                    Globals.MemeLog(MethodBase.GetCurrentMethod(), www.downloadHandler.text);
                    UIStateManager.instance.ContextualRoomLoad(UIStateChange.TransitionType.Backward, true, false);
                }
            }
        }
    }

    // KICK PLAYER
    public IEnumerator DeletePlayerFromRoom(Room room, UserSummaryOut user)
    {
        using (UnityWebRequest www = InternalWebRequest.createDelete(gameroomsSuffix + "/" + room.name + "/" + userSuffix + "/" + user.email))
        {
            yield return www.SendWebRequest();
            CallComplete();

            if (www.isNetworkError)
            {
                Debug.LogError(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "DeletePlayerFromRoom", true);
            }
            else
            {
                if( www.downloadHandler.text.ToLower().Contains("unauthorized"))
                {
                    Debug.LogError("Can't Kick, Unauthorized.");
                    LoadingCircle.instance.Toggle(false, "DeletePlayerFromRoom, unauthorized");
                }
                else
                {
                    Globals.MemeLog(MethodBase.GetCurrentMethod(), www.downloadHandler.text);
                    Debug.Log("Player " + user.displayName + " was kicked from room. " + Time.time);
                    GetGameRoomCR = StartCoroutine(GetGameRoom(room.name));
                }
            }
        }
    }

    public IEnumerator DeleteRoom (Room room)
	{
        using (UnityWebRequest www = InternalWebRequest.createDelete(gameroomsSuffix + "/" + room.name))
        {
            CallLog?.Invoke(Calls.DeleteChannel, CallTrigger.Start);
            yield return www.SendWebRequest();
            CallLog?.Invoke(Calls.DeleteChannel, CallTrigger.End);
            CallComplete();

            if (www.isNetworkError)
            {
                Debug.LogError(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "DeleteRoom", true);
            }
            else
            {
                // Show results as text
                Debug.Log("Room " + room.name + " deleted. " + Time.time);
                GetGameRoomsCR = StartCoroutine(GetGameRooms(null));
            }
        }
	}

    public IEnumerator DeleteGameFromRoom(string gameId)
    {
        using (UnityWebRequest www = InternalWebRequest.createDelete(gameSuffix + "/" + gameId))
        {
            CallLog?.Invoke(Calls.DeleteGame, CallTrigger.Start);
            yield return www.SendWebRequest();
            yield return new WaitForSeconds(1f);
            CallLog?.Invoke(Calls.DeleteGame, CallTrigger.End);
            CallComplete();

            if (www.isNetworkError)
            {
                Debug.LogError(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "DeleteGameFromRoom", true);
            }
            else
            {
                // Show results as text
                Debug.Log("Game " + gameId + " was deleted. " + Time.time);

                UIStateManager.instance.ContextualRoomLoad(UIStateChange.TransitionType.Backward, true);
                LoadingCircle.instance.Toggle(false, "DeleteGameFromRoom, success");
            }
        }
    }

    public static string newRoomJoinName = null;
	public IEnumerator JoinGameRoom (string roomName, string pin)
	{
        roomName = roomName.ToLower();

		GameRoomIn roomIn = new GameRoomIn();
		roomIn.name = roomName;

        string gameRoomJson = JsonUtility.ToJson(roomIn);

        using (UnityWebRequest www = InternalWebRequest.createPost(gameroomsSuffix + "/" + roomName + "/join/" + pin, gameRoomJson))
        {
            yield return www.SendWebRequest();
            CallComplete();
            if (www.isNetworkError)
            {
                Debug.LogError(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "JoinGameRoom", true);
            }
            else
            {
                JoinRoomResponse joinRoomResponse = new JoinRoomResponse();
                joinRoomResponse = JsonUtility.FromJson<JoinRoomResponse>(www.downloadHandler.text);

                if (joinRoomResponse != null)
                {
                    if (joinRoomResponse.message.Contains("Nope"))
                        StartCoroutine(ResolveJoinRoomFail());
                }
                else
                {
                    newRoomJoinName = roomName;
                    GetGameRoomsCR = StartCoroutine(GetGameRooms(roomName));
                }
            }
        }
	}

    private IEnumerator ResolveJoinRoomFail()
    {
        yield return new WaitForSeconds(2f);
        print("Nope. Join failed.");
        Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.CannotJoinRoom);
        LoadingCircle.instance.Toggle(false, "ResolveJoinRoomFail");
    }

    public IEnumerator PostGameRoom (string roomName)
	{
        GameRoomIn roomIn = new GameRoomIn();
        roomIn.name = roomName;

        string gameRoomJson = JsonUtility.ToJson(roomIn);
        print("gameRoomJson: " + gameRoomJson);

        CallLog?.Invoke(Calls.CreateRoom, CallTrigger.Start);
        using (UnityWebRequest www = InternalWebRequest.createPost(gameroomsSuffix, gameRoomJson))
        {
            yield return www.SendWebRequest();
            CallComplete();
            yield return new WaitForSeconds(1f);
            CallLog?.Invoke(Calls.CreateRoom, CallTrigger.End);

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false, "PostGameRoom", true);
            }
            else
            {
                CreateRoomResponse createRoomResponse = new CreateRoomResponse();
                createRoomResponse = JsonUtility.FromJson<CreateRoomResponse>(www.downloadHandler.text);

                if (createRoomResponse.statusCode == 500)
                    StartCoroutine(HandleRoomCreationError());
                else
                {
                    GameRoomOut gameRoomOut = JsonUtility.FromJson<GameRoomOut>(www.downloadHandler.text);
                    string code = gameRoomOut.code;
                    yield return GetGameRoomsCR = StartCoroutine(GetGameRooms(roomName));
                    yield return new WaitForSeconds(0.75f);
                    NewRoomCreated?.Invoke(roomName, code );
                }
            }
        }
    }

    private IEnumerator HandleRoomCreationError()
    {
        yield return new WaitForSeconds(2f);
        LoadingCircle.instance.Toggle(false, "HandleRoomCreationError");
        Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.CannotCreateRoom);
    }

    public IEnumerator PostGameToGameRoom (Room room, string[] players, int wTotal = 10, bool toGameRound = false, int numOfRandomUsers = 0)
	{
        if (room.games != null)
        {
            GameIn gameIn = new GameIn
            {
                handSize = Globals.handSize,
                players = players,
                randomUsers = numOfRandomUsers,
                winTotal = wTotal
            };

            string gameInJson = JsonUtility.ToJson(gameIn);
            print("gameInJson: " + gameInJson);

            using (UnityWebRequest www = InternalWebRequest.createPost(gameroomsSuffix + "/" + room.name + "/" + gameSuffix, gameInJson))
            {
                yield return www.SendWebRequest();
                CallComplete();
                if (www.isNetworkError)
                {
                    Debug.Log(www.error);
                    DebugPanel.instance.Show(www.error, -1);
                    LoadingCircle.instance.Toggle(false, "PostGameToGameRoom", true);
                }
                else
                {
                    GameSummaryOut gameSummaryOut = JsonUtility.FromJson<GameSummaryOut>(www.downloadHandler.text);
                    if (www.downloadHandler.text.Contains("statusCode\":500"))
                    {
                        Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.ServerError);
                        Debug.LogError("Status Code 500 returned for PostGameToGameRoom.");
                        CallLog.Invoke(Calls.CreatingGame, CallTrigger.End);
                        LoadingCircle.instance.Toggle(false, "PostGameToGameRoom, Code 500");
                    }
                    else
                        RESTInterface.instance.StartCoroutine(PostRoundToGame(gameSummaryOut.id));
                }
            }
        }
    }

	public IEnumerator GetGameDetail (string gameId, bool toGameRound = false, Room room = null, UIStateManager.StateNames currentState = UIStateManager.StateNames.GameLobby)
	{
        bool forSummary = DataManager.GetCoreFlow() != DataManager.CoreFlow.Standard;

        Calls callForModel = !forSummary ? Calls.GetGameDetail : Calls.GetGameDetailForSummary;
        if (DataManager.GetCoreFlow() == DataManager.CoreFlow.FinalVoteOfRound)
            callForModel = Calls.TallyingResults;

        if( !toGameRound )
		    LoadingCircle.instance.Toggle (true);

        using (UnityWebRequest www = InternalWebRequest.createGet(gameSuffix + "/" + gameId))
		{
            if( UIStateManager.instance.GetCurrentState() != UIStateManager.StateNames.Leaderboard )
                CallLog?.Invoke(callForModel, CallTrigger.Start);
            yield return www.SendWebRequest();
            CallComplete();
            yield return new WaitForSeconds(0.8f);
            if ( !forSummary )
                CallLog?.Invoke(callForModel, CallTrigger.End);
            yield return new WaitForSeconds(0.3f);

            if (www.isNetworkError)
			{
				Debug.Log (www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            } else
			{				
				GameDetailOut gameDetailOut = JsonUtility.FromJson<GameDetailOut> (www.downloadHandler.text);
                RetrievedGameDetail?.Invoke(gameDetailOut, toGameRound, room, currentState);
            }
		}
	}

    public IEnumerator PostRoundToGame (string gameId)
	{
        // toGameRound ALWAYS TRUE NOW. WHEN THIS IS THE CASE, ROOM IS ALWAYS NULL

        using (UnityWebRequest www = InternalWebRequest.createPost(gameSuffix + "/" + gameId + startRoundSuffix, null))
        {
            yield return www.SendWebRequest();
            CallComplete();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                Globals.MemeLog(MethodBase.GetCurrentMethod(), "A round was posted to game: " + gameId);
                CallLog?.Invoke(Calls.CreatingGame, CallTrigger.End);
                RESTInterface.instance.StartCoroutine(GetGameDetail(gameId, true, null));
            }
        }
	}

	public IEnumerator PostChatToGameRoom (string accessToken, string roomId, string message, Globals.LoginStatusOptions loginMethod = Globals.LoginStatusOptions.Facebook)
	{
		ChatIn chatIn = new ChatIn();
		chatIn.message = message;

		string chatInJson = JsonUtility.ToJson(chatIn);
		print ("chatInJson: " + chatInJson);

        using (UnityWebRequest www = InternalWebRequest.createPost(gameroomsSuffix + "/" + roomId + "/" + chatSuffix, chatInJson))
        {
            CallLog?.Invoke(Calls.PostChatToGameRoom, CallTrigger.Start);
            yield return www.SendWebRequest();
            CallComplete();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                // Show results as text
                // Debug.Log("Return response: " + www.downloadHandler.text);
                ChatPosted?.Invoke(roomId);
            }
        }
	}

    public IEnumerator GetUser (bool fromAutoAuth = false, Globals.LoginHeaderTags headerTags = Globals.LoginHeaderTags.dbpe)
	{
        if (DataManager.GetInternetStatus(RESTInterface.Calls.GetUser))
        {
            using (UnityWebRequest www = InternalWebRequest.createGet(usersSuffix + usersGetSuffix))
            {
                yield return www.SendWebRequest();
                CallComplete();

                if (www.isNetworkError)
                {
                    EvaluateError(www.error);
                    DebugPanel.instance.Show(www.error, -1);
                    LoadingCircle.instance.Toggle(false);
                }
                else
                {
                    Globals.loggedIn = Globals.LoginStatusOptions.UniqueLoginGUID;
                    Debug.Log("GetUser Response: " + www.downloadHandler.text);

                    UserOut userInfo = JsonUtility.FromJson<UserOut>(www.downloadHandler.text);
                    Globals.user = userInfo;

                    // EMAIL
                    Globals.email = userInfo.email;

                    // DISPLAY NAME
                    Globals.displayName = userInfo.displayName;
                    PushDisplayName?.Invoke(userInfo.displayName);

                    // ALLOW NOTIFICATIONS
                    Globals.allowNotifications = userInfo.allowNotifications;
                    if (Application.platform == RuntimePlatform.Android)
                        Globals.allowNotifications = true;
                    //Globals.MemeLog(MethodBase.GetCurrentMethod(), "Allow Notifications is: " + Globals.allowNotifications);

                    // TOKENS
                    Globals.tokens = userInfo.tokens;

                    // AVATAR MODIFIED
                    Globals.avatarModified = userInfo.avatarModified;
                    //Globals.MemeLog(MethodBase.GetCurrentMethod(), "Avatar Modified is: " + Globals.avatarModified);

                    // CREATED
                    Globals.created = userInfo.created;
                    //Globals.MemeLog(MethodBase.GetCurrentMethod(), "Created is: " + Globals.created);

                    // MEMBER
                    if (userInfo.member == null || userInfo.member == "")
                        Globals.member = 0;
                    else
                        Globals.member = Convert.ToInt32(userInfo.member);
                    Globals.MemeLog(MethodBase.GetCurrentMethod(), "Member is: " + Globals.member);

                    // CURRENCY
                    Globals.MemeLog(MethodBase.GetCurrentMethod(), "Currency is: " + userInfo.currency);
                    if (userInfo.currency == null || userInfo.currency == "")
                        Globals.SetCurrentCurrency(0);
                    else
                        Globals.SetCurrentCurrency(Convert.ToInt32(userInfo.currency));

                    // FAVORITES
                    string favoritesStr = userInfo.favorites;
                    if (favoritesStr == null)
                        favoritesStr = "";
                    List<string> favoritesIDs = new List<string>();
                    if (favoritesStr != "")
                        favoritesIDs = DataManager.GetStringListFromCommaDelimited(favoritesStr);
                    GifManager.instance.CheckForFavorites(favoritesIDs);

                    if (PlayerPrefsInterface.GetDisplayName() != userInfo.displayName)
                        PlayerPrefsInterface.SetDisplayName(userInfo.displayName);
                    if (Globals.loggedIn == Globals.LoginStatusOptions.DebugEmailBypass)
                    {
                        if (!string.IsNullOrEmpty(userInfo.email))
                        {
                            PlayerPrefsInterface.SetDebugBypassEmail(Globals.keyAccess);
                            UIStateManager.instance.ChangeState(UIStateManager.StateNames.MainMenu);
                        }
                        else if (LoginFailedEvent != null)
                        {
                            Globals.MemeLog(MethodBase.GetCurrentMethod(), "Fire login fail event.");
                            LoginFailedEvent();
                        }
                    }
                    else
                    {
                        if (fromAutoAuth)
                            MainMenuAutoAuth?.Invoke();
                    }
                }
            }
        }
	}

    public void SNSRegDevice()
    {
        SNSRegisterDevice?.Invoke();
    }

	public enum PutUserReason
	{
		SetDisplayName,
		SetAllowNotifications,
		SetDeviceEndpoint,
        AvatarModified,
        SetMemberStatus,
        UpdateCurrency,
        UpdateFavorites
	}

	// FOR ENDPOINT TOKENS
	public IEnumerator PutUserSetTokens (TokenOut[] tokens)
	{
        UserIn userInInfo = DataManager.ConfigureUserInInfo(null, null, tokens);
        RESTInterface.instance.StartCoroutine(PutUser(PutUserReason.SetDeviceEndpoint, userInInfo, true));

        yield return null;
    }

    public IEnumerator PutUserEditDisplayName()
    {
        LoadingCircle.instance.Toggle(true);
        UserIn userInInfo = DataManager.ConfigureUserInInfo();
        yield return RESTInterface.instance.StartCoroutine(PutUser(PutUserReason.SetDisplayName, userInInfo, false));
    }

	// FOR DISPLAY NAME & ALLOWNOTIFICATIONS
	public IEnumerator PutUser (PutUserReason reason, string displayName, bool allowNotifications, Globals.LoginStatusOptions loginMethod = Globals.LoginStatusOptions.UniqueLoginGUID, bool initial = false)
	{
        UserIn userInInfo = DataManager.ConfigureUserInInfo(displayName, allowNotifications);
        yield return RESTInterface.instance.StartCoroutine(PutUser(reason, userInInfo, initial));
	}

    // FOR AVATAR MODIFIED
    public IEnumerator PutUserAvatarModified(long avatarModified)
    {
        UserIn userInInfo = DataManager.ConfigureUserInInfo(null, null, null, avatarModified);
        yield return RESTInterface.instance.StartCoroutine(PutUser(PutUserReason.AvatarModified, userInInfo));
    }

    // SET MEMBER STATUS
    public IEnumerator PutUserSetMemberStatus(int member)
    {
        UserIn userInInfo = DataManager.ConfigureUserInInfo(null, null, null, null, member);
        yield return RESTInterface.instance.StartCoroutine(PutUser(PutUserReason.SetMemberStatus, userInInfo));
    }

    public void UpdateCurrency(int delta)
    {
        StartCoroutine(PutUserCurrencyUpdate(delta));
    }

    public IEnumerator SetMemberStatusRoutine(Globals.MemberNum memberNum)
    {
        yield return StartCoroutine(PutUserSetMemberStatus((int)memberNum));
    }

    public void SetMemberStatus(Globals.MemberNum memberNum)
    {
        StartCoroutine(PutUserSetMemberStatus((int)memberNum));
    }

    // UPDATE CURRENCY
    public IEnumerator PutUserCurrencyUpdate(int currencyDelta)
    {
        int currentCurrency = Globals.GetCurrentCurrency();
        int newCurrency = currentCurrency + currencyDelta;
        UserIn userInInfo = DataManager.ConfigureUserInInfo(null, null, null, null, null, newCurrency);
        yield return RESTInterface.instance.StartCoroutine(PutUser(PutUserReason.UpdateCurrency, userInInfo));
    }

    // SET FAVORITES
    public void UpdateFavorites(List<string> favList)
    {
        StartCoroutine(PutUserSetFavorites(favList));
    }

    private IEnumerator PutUserSetFavorites(List<string> favorites)
    {
        string favoritesStr = DataManager.SetCommaBasedString(favorites);
        UserIn userInInfo = DataManager.ConfigureUserInInfo(null, null, null, null, null, null, favoritesStr);
        yield return RESTInterface.instance.StartCoroutine(PutUser(PutUserReason.UpdateFavorites, userInInfo));
    }

    // NEW MASTER WHICH ACCEPTS 'userInInfo'
    // THIS HELPS TO ALWAYS SUBMIT VALID FIELD DATA FOR ALL USER FIELDS, WHEN UPDATING JUST A SINGLE USER FIELD, FOR INSTANCE.
    private IEnumerator PutUser(PutUserReason reason, UserIn userInInfo, bool initial = false)
    {
        string userJson = JsonUtility.ToJson(userInInfo);
        Globals.MemeLog(MethodBase.GetCurrentMethod(), "userJson: " + userJson);

        using (UnityWebRequest www = InternalWebRequest.createPut(usersSuffix, userJson))
        {
            yield return www.SendWebRequest();
            CallComplete();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                // Show results as text
                Globals.MemeLog(MethodBase.GetCurrentMethod(), "Return response: " + www.downloadHandler.text);

                UserOut userInfo = JsonUtility.FromJson<UserOut>(www.downloadHandler.text);
                if (userInfo == null)
                    Debug.Log("userInfo is null. Why?");

                PlayerPrefsInterface.SetDisplayName(userInInfo.displayName);
                Debug.Log("Seeing displayName outbound: " + userInInfo.displayName);
                PushDisplayName?.Invoke(userInInfo.displayName);
                Globals.LoginStatusOptions priorLoginStatus = Globals.loggedIn;
                Globals.loggedIn = Globals.loginMethod;
                if (Globals.loginMethod == Globals.LoginStatusOptions.UniqueLoginGUID)
                    Globals.email = Globals.loginID;

                Globals.displayName = userInInfo.displayName;
                Globals.allowNotifications = userInInfo.allowNotifications;

                switch (reason)
                {
                    case PutUserReason.SetDisplayName:

                        UIStateManager.instance.ChangeState(UIStateManager.StateNames.User, UIStateChange.TransitionType.Backward, false, false);
                        LoadingCircle.instance.Toggle(false);
                        break;

                    case PutUserReason.SetAllowNotifications:
                        Globals.MemeLog(MethodBase.GetCurrentMethod(), "PutUser allowNotifications set success: " + userInInfo.allowNotifications + "  " + Time.time);

                        if (Application.isEditor)
                            DBUserCreatedEvent?.Invoke(true);
                        else
                            SNSRegisterDevice?.Invoke();
                        break;

                    case PutUserReason.SetDeviceEndpoint:
                        if( priorLoginStatus == Globals.LoginStatusOptions.NotLoggedIn )
                            DBUserCreatedEvent?.Invoke(false);
                        Globals.MemeLog(MethodBase.GetCurrentMethod(), "PutUser endpoint added to account success. " + Time.time);

                        LoadingCircle.instance.Toggle(false);
                        break;

                    case PutUserReason.AvatarModified:
                        AvatarModifyComplete?.Invoke();
                        break;

                    case PutUserReason.SetMemberStatus:
                        Globals.member = Convert.ToInt32(userInInfo.member);
                        if (Globals.member == -1)
                        {
                            yield return new WaitForSeconds(1f);
                            Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.NewPlayerGraceEnded);
                        }      
                        break;
                    case PutUserReason.UpdateCurrency:
                        int current = Convert.ToInt32(userInInfo.currency);
                        Globals.SetCurrentCurrency(current);
                        break;
                    case PutUserReason.UpdateFavorites:
                        Globals.favoritesIDs = DataManager.GetStringListFromCommaDelimited(userInInfo.favorites);
                        LoadingCircle.instance.Toggle(true);
                        FavoritesUpdate?.Invoke();
                        break;
                }
            }
            LoadingCircle.instance.Toggle(false);
        }
    }

    public IEnumerator SetGameRoomSettings(Room room, RoomPlayerIn roomPlayerIn)
    {
        string roomPlayerInJSON = JsonUtility.ToJson(roomPlayerIn);
        Globals.MemeLog(MethodBase.GetCurrentMethod(), "roomPlayerInJSON: " + roomPlayerInJSON);

        using (UnityWebRequest www = InternalWebRequest.createPost(gameRoomPlayersSuffix + "/" + room.name + settingsSuffix, roomPlayerInJSON))
        {
            yield return www.SendWebRequest();
            CallComplete();

            yield return new WaitForSeconds(0.3f);

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                RoomSettingsUpdateSuccess?.Invoke(room, roomPlayerIn);
                Globals.MemeLog(MethodBase.GetCurrentMethod(), "Room Settings Update Success.");  
            }
        }
    }

    public IEnumerator PlayCard (string gameId, int round, string cardId, PlayCardIn playCardIn)
	{
        string playCardInJSON = JsonUtility.ToJson(playCardIn);
        Globals.MemeLog(MethodBase.GetCurrentMethod(), "playCardInJSON: " + playCardInJSON);

        using (UnityWebRequest www = InternalWebRequest.createPost(gameSuffix + "/" + gameId + "/round/" + round.ToString() + "/playCard/" + cardId, playCardInJSON))
        {
            yield return www.SendWebRequest();
            CallComplete();
            yield return new WaitForSeconds(0.3f);

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                PlayCardSuccess?.Invoke(gameId);
                Globals.MemeLog(MethodBase.GetCurrentMethod(), "An answer card was played: " + gameId + " of round: " + round.ToString());
                GameLobby.actionCompleted = gameId;

                yield return GetGameRoomsCR = StartCoroutine(GetGameRooms(null));    
            }
        }
	}

    public IEnumerator VoteCard (string gameId, int round, string cardId, GameDetailOut currDetailOut)
	{
        using (UnityWebRequest www = InternalWebRequest.createPost(gameSuffix + "/" + gameId + "/round/" + round.ToString() + "/voteCard/" + cardId, null))
        {
            CallLog?.Invoke(Calls.VoteCard, CallTrigger.Start);
            yield return www.SendWebRequest();
            CallComplete();
            CallLog?.Invoke(Calls.VoteCard, CallTrigger.End);
            yield return new WaitForSeconds(0.3f);

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                Globals.MemeLog(MethodBase.GetCurrentMethod(), "VoteCard return response: " + www.downloadHandler.text);
                GameLobby.actionCompleted = gameId;

                if (DataManager.IsFinalVote(currDetailOut.rounds[round - 1], currDetailOut.players.Length))
                {
                    // SETUP SUMMARY INFO
                    RoundSummary.SummaryState state = new RoundSummary.SummaryState();
                    state.binType = GameLobby.BinType.ActionRequired;
                    state.summaryRound = round;
                    BroadcastFinalVoteSummaryInfo?.Invoke(state);

                    DataManager.SetCoreFlow(DataManager.CoreFlow.FinalVoteOfRound);

                    Room room = Leaderboard.lastRoom;
                    room.showRoundSummary = currDetailOut.round;
                    SendToCallLog(Calls.TallyingResults, CallTrigger.Start);
                    SoundManager.instance.Play(SoundManager.SoundElementNames.DrumRoll, 0.4f);

                    StartCoroutine(GetGameDetail(gameId, false, room, UIStateManager.StateNames.Voting));
                }
                else
                {
                    VoteCardSuccess?.Invoke(gameId);
                    yield return GetGameRoomsCR = StartCoroutine(GetGameRooms(null));
                }
            }
        }
	}

    public IEnumerator TradeInCard(string gameId, string cardId)
    {
        using (UnityWebRequest www = InternalWebRequest.createPost(gameSuffix + "/" + gameId + "/tradeIn/" + cardId, null))
        {
            CallLog?.Invoke(Calls.TradeInCard, CallTrigger.Start);
            yield return www.SendWebRequest();
            CallComplete();
            yield return new WaitForSeconds(1f);
            CallLog?.Invoke(Calls.TradeInCard, CallTrigger.End);

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
                DebugPanel.instance.Show(www.error, -1);
                LoadingCircle.instance.Toggle(false);
            }
            else
            {
                GameDetailOut gameDetailOut = JsonUtility.FromJson<GameDetailOut>(www.downloadHandler.text);
                print(www.downloadHandler.text);

                TradeInCardSuccess?.Invoke(gameDetailOut);
                Globals.MemeLog(MethodBase.GetCurrentMethod(), "TradeIn Card Success!");
                GameLobby.actionCompleted = gameId;
            }
        }
    }

    public IEnumerator PostSystemVariable(string key, string value)
    {
        if(DataManager.GetInternetStatus(RESTInterface.Calls.PostSystemVariable))
        {
            VariableIn variableIn = new VariableIn();
            variableIn.key = key;
            variableIn.value = value;
            string systemVariableInJSON = JsonUtility.ToJson(variableIn);

            using (UnityWebRequest www = InternalWebRequest.createPost(systemSuffix + variableSuffix, systemVariableInJSON))
            {
                yield return www.SendWebRequest();
                CallComplete();
                if (www.isNetworkError)
                {
                    Debug.Log("Network Error for System Variable Post.");
                }
                else
                {
                    Debug.Log("System Variable: " + key + " has been set to: " + value);
                }
            }
        }
    }

    public IEnumerator GetSystemVariables(bool admin = false)
    {
        if ((Globals.email != null && Globals.email != "" && Globals.keyAccess != "") || admin)
        {
            if (DataManager.GetInternetStatus(RESTInterface.Calls.GetSystemVariables))
            {
                using (UnityWebRequest www = InternalWebRequest.createGet(systemSuffix + variablesSuffix, admin))
                {
                    yield return www.SendWebRequest();
                    CallComplete();

                    if (www.isNetworkError)
                    {
                        EvaluateError(www.error);
                        DebugPanel.instance.Show(www.error, -1);
                        LoadingCircle.instance.Toggle(false);
                    }
                    else
                    {
                        Globals.MemeLog(MethodBase.GetCurrentMethod(), "Response: " + www.downloadHandler.text);
                        Variable[] vars = new Variable[0];
                        if (www.downloadHandler.text != "[]")
                        {
                            vars = JsonHelper.getJsonArray<Variable>(www.downloadHandler.text);
                            BroadcastSystemVariables?.Invoke(vars);
                        }
                        else
                            Globals.MemeLog(MethodBase.GetCurrentMethod(), "System Variables list is reporting empty. Please fix.");
                    }
                }
            }
        }  
    }

    // STRUCTURES TO MATCH SWAGGER DIRECTLY
    // ::::::::::::::::::::::::::::::::::::

    // AUDIO

    [System.Serializable]
    public class AnswerCardOut
    {
        public string id;
        public string english;
        public string searchTerm;
    }

    [System.Serializable]
    public class AnswerOut
    {
        public AnswerCardOut answerCard;
        public string url;
        public UserOut user;
        //public AnswerCardOut vote;
    }

    [System.Serializable]
    public class GameDetailOut
    {
        public string id;
        public long deleted;
        public AnswerCardOut[] discarded;
        public int drawPileSize;
        public long ended;
        public long modified;
        public PlayerOut[] players;
        public int questionHourLimit;
        public int round;
        public RoundOut[] rounds;
        public long started;
        public int voteHourLimit;
        public PlayerSummaryOut winner;
        public int winTotal;
    }

    [System.Serializable]
    public class Variable
    {
        public string key;
        public string value;
    }

    [System.Serializable]
    public class PlayerOut
    {
        public int currentRoundSwapCount;
        public AnswerCardOut[] hand;
        public int totalVotes;
        public UserOut userOut;
    }

    [System.Serializable]
    public class PlayerSummaryOut
    {
        public int totalVotes;
        public UserSummaryOut userOut;
    }

    [System.Serializable]
    public class QuestionCardOut
    {
        public string id;
        public string english;
    }

    [System.Serializable]
    public class VoteOut
    {
        public string castUserId;
        public string forUserId;
    }

    [System.Serializable]
    public class RoundOut
    {
        public AnswerOut[] answers;
        public QuestionCardOut questionCard;
        public long questionStarted;
        public int status;
        public VoteOut[] votes;
        public int voteStarted;
        public long questionElapsed;
        public long voteElapsed;
    }

    [System.Serializable]
    public class TokenOut
    {
        public string type;
        public string endpointArn;
        public string subscriptionArn;
    }

    [System.Serializable]
    public class UserOut
    {
        public bool allowNotifications;
        public long avatarModified;
        public long created;
        public string currency;
        public string displayName;
        public string email;
        public string favorites;
        public string member;
        public TokenOut[] tokens;
        public string type;
    }

    [System.Serializable]
    public class UserSummaryOut
    {
        public long avatarModified;
        public long created;
        public string displayName;
        public string email;
        public string member;
    }

    //[System.Serializable]
    public class PlayCardIn
    {
        public string url;
    }

    [System.Serializable]
    public class AnswerSummaryOut
    {
        public UserSummaryOut user;
    }

    [System.Serializable]
    public class ChatOut
    {
        public string date;
        public string message;
        public UserOut user;
    }

    [System.Serializable]
    public class GameRoomOut
    {
        public string id;
        public string code;
        public string displayName;
        public string name;
        public UserSummaryOut owner;
        public ChatOut[] chatter;
        public GameSummaryOut[] games;
        public RoomPlayerOut[] roomPlayers;
    }

    [System.Serializable]
    public class RoomPlayerIn
    {
        public bool availableForNextGame;
        public int gameDelayHours;
        public bool notifyForChat;
        public bool notifyForOther;
    }

    [System.Serializable]
    public class RoomPlayerOut
    {
        public bool availableForNextGame;
        public int gameDelayHours;
        public bool notifyForChat;
        public bool notifyForOther;
        public UserSummaryOut user;
        public bool neutered = false;
    }

    [System.Serializable]
    public class GameSummaryOut
    {
        public string id;
        public RoundSummaryOut currentRound;
        public long deleted;
        public long ended;
        public long modified;
        public int round;
        public PlayerSummaryOut[] players;
        public int questionHourLimit;
        public long started;
        public int voteHourLimit;
        public PlayerSummaryOut winner;
    }

    [System.Serializable]
    public class RoundSummaryOut
    {
        public AnswerSummaryOut[] answers;
        public long questionStarted;
        public int status;  //0 - Questions, 1 - Votin, 2 - Complete
        public VoteOut[] votes;
        public long voteStarted;
        public long questionElapsed;
        public long voteElapsed;
    }

    public class GameRoomIn
    {
        public string name;
    }

    // SIMPLEBEAN???

    public class ChatIn
    {
        public string message;
    }

    public class GameIn
    {
        public int handSize;
        public string[] players;
        public int randomUsers;
        public int winTotal;
    }

    public class VariableIn
    {
        public string key;
        public string value;
    }

    // TIMEZONE

    [System.Serializable]
    public class JoinRoomResponse
    {
        public string date;
        public string message;
    }

    [System.Serializable]
    public class CreateRoomResponse
    {
        public int statusCode;
        public string statusDescription;
        public string errorMessage;
    }

    [System.Serializable]
    public class UserIn
    {
        public bool allowNotifications;
        public long avatarModified;
        public string currency;
        public string displayName;
        public string favorites;
        public string member;
        public TokenOut[] tokens;
    }

    // ::::::::::::::::::::::::::::::
    // END SWAGGER MATCHED STRUCTURES

    public class JsonHelper
	{
	    public static T[] getJsonArray<T>(string json)
	    {
	        string newJson = "{ \"array\": " + json + "}";
	        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>> (newJson);
	        return wrapper.array;
	    }
	 
	    [System.Serializable]
	    private class Wrapper<T>
	    {
	        public T[] array;
	    }
	}

	private void EvaluateError (string error, RESTInterface.Calls call = RESTInterface.Calls.GetUser)
	{
        Popup.instance.Spawn(error, call);
    }

	private void ReportRooms (GameRoomOut[] rooms, string accessToken, string newRoomName, Globals.LoginStatusOptions loginMethod)
	{
		string str = "";
        if( rooms == null )
        {
            Debug.Log("ROOMS IS NULL!, WHY?");
            return;
        }
		for (int i = 0; i < rooms.Length; i++)  //NULL REF HERE?
		{
			str = "Room " + i.ToString ();
			str += ", displayName: " + rooms [i].displayName;
			str += ", owner: " + rooms [i].owner.displayName;

			if (rooms[i].roomPlayers != null)
			{
				if (rooms[i].roomPlayers.Length > 0)
				{
					str += ", Members [" + rooms[i].roomPlayers.Length + "]: ";
					for (int j = 0; j < rooms[i].roomPlayers.Length; j++)
					{
						if (j != 0)
							str += ", ";
						str += rooms[i].roomPlayers[j].user.email;
					}
				} else
					str += " No members.";
			} else
			{
				str += " Players is null.";
			}

            if (rooms[i].games.Length > 0)
            {
                GameRoomOut thisRoom = rooms[i];
                GameSummaryOut currentGame = DataManager.GetCurrentGame(thisRoom);
                str += " RoomId: " + rooms[i].id;
                str += " GameId: " + currentGame.id;
                str += " Round: " + currentGame.round.ToString();
                RoundSummaryOut currentRoundSummary = currentGame.currentRound;
                str += " Round Status: " + currentRoundSummary.status;
                if( currentRoundSummary.answers != null )
                    str += " Answer Count: " + currentRoundSummary.answers.Length;

                if( currentGame.currentRound.votes != null )
                {
                    for( int l = 0; l < currentGame.currentRound.votes.Length; l++ )
                        str += "Vote cast for: " + currentGame.currentRound.votes[l].castUserId;
                }
                str += " Game Winner: " + currentGame.winner.userOut.displayName;
            }
            else
            {
                str += " RoomId: " + rooms[i].id;
                str += " No games.";
            }
            Globals.MemeLog(MethodBase.GetCurrentMethod(), str);
		}
	}

    Texture myTexture;
    public IEnumerator GetAvatarTexture(string email, System.Action<Texture> gotAvatar, bool getCustomAvatar)
    {
        while (SystemVarsManager.instance == null)
            yield return null;

        string dynamicAPIstring = null;
        while( dynamicAPIstring == null )
        {
            dynamicAPIstring = SystemVarsManager.instance.GetString(SystemVarsManager.VariablesList.avatarAPI);
            yield return null;
        }  

        int pixelSize = Avatar.pixelSize;
        string md5Hash = DataManager.GetMd5Hash(email);

        string URL = "";
        if (getCustomAvatar)
        {
            Debug.Log("Attempting to get custom from S3 here then for: " + email);

            //https://quipple-avatars.s3-us-west-2.amazonaws.com/33840938985426cd1609609034de6fd7.png
            URL = String.Format("https://quipple-avatars.s3-us-west-2.amazonaws.com/{0}.png", md5Hash);

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(URL);
            yield return www.SendWebRequest();
            CallComplete();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture tex = ((DownloadHandlerTexture)www.downloadHandler).texture;
                gotAvatar?.Invoke(tex);
            }
        }
        else
        {
            // GRAVATAR / WAVATAR
            //URL = String.Format("https://www.gravatar.com/avatar/{0}?s={1}&d=wavatar&f=y", md5Hash, pixelSize);
            URL = String.Format(dynamicAPIstring, md5Hash, pixelSize);

            using (UnityWebRequest www = UnityWebRequest.Get(URL))
            {
                DownloadHandlerTexture tex = new DownloadHandlerTexture();
                www.downloadHandler = tex;

                yield return www.SendWebRequest();
                CallComplete();

                if (www.isNetworkError)
                {

                    Debug.Log(www.error);
                    DebugPanel.instance.Show(www.error, -1);
                    LoadingCircle.instance.Toggle(false);

                    /*
                Debug.Log("We are not pulling Adorable.IO texture, this is bad.");
                myTexture = noConnectionAvatarTexture;
                gotAvatar?.Invoke(myTexture);
                    */
                }
                else
                {
                    while (www.downloadProgress < 1.0)
                        yield return null;
                    while (www.downloadHandler == null)
                        yield return null;

                    myTexture = tex.texture;
                    gotAvatar?.Invoke(myTexture);
                }
            }
        }
    }

    public void TurnOnNotifications()
    {
        SNSRegisterDevice?.Invoke();
    }

    public void SendToCallLog(Calls call, CallTrigger trigger)
    {
        CallLog?.Invoke(call, trigger);
    }
}
