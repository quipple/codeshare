﻿// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// THIS SCRIPT IS FOR THE MAIN INSTANTIATED CONTENT BLOCK SEEN ON THE GAME LOBBY SCREEN
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Reflection;

public class GameBlock : MonoBehaviour
{
    public const bool showMySubmission = false;

    private const bool debugTestDate = false;
    private const string resultsAreIn_Str = "Results are in!";

    private Animator anim;

    public delegate void SetupGameRoomDelegate(Room room);
    public static event SetupGameRoomDelegate BroadcastRoom;

    public delegate void GameBecameReadyForVotingDelegate(Room room);
    public static event GameBecameReadyForVotingDelegate GameBecameReadyForVoting;

    public delegate void BroadcastSummaryInfoDelegate(RoundSummary.SummaryState summaryState);
    public static event BroadcastSummaryInfoDelegate BroadcastSummaryInfo;

    public delegate void ShowInterstitialAdDelegate();
    public static event ShowInterstitialAdDelegate ShowInterstitialAd;

    public delegate void TestWinnerScenarioDelegate(List<RESTInterface.PlayerOut>[] playerPockets);
    public static event TestWinnerScenarioDelegate TestWinnerScenarioEvent;

    // ----------------------------------------
    // EVENT REGISTRATION
    void OnEnable() { RegisterEvents(true); }
    void OnDisable() { RegisterEvents(false); }
    void OnDestroy() { RegisterEvents(false); }
    private void RegisterEvents(bool register)
    {
        if (register)
        {
            UIStateManager.StateChangeEvent += StateChangeCompleteListener;
            RESTInterface.RetrievedGameDetail += RetrievedGameDetailListener;
        }
        else
        {
            UIStateManager.StateChangeEvent -= StateChangeCompleteListener;
            RESTInterface.RetrievedGameDetail -= RetrievedGameDetailListener;
        }
    }
    // ----------------------------------------

    private int roundPhaseStartTimeEpoc;

    public class StasisStartInfo
    {
        public int days;
        public int hours;
        public int minutes;
    }
    private StasisStartInfo stasisStartInfo;

    public class TimestampStorage
    {
        // KEY USES GameId & UserId
        public string timestamp;
        public string currentRoundState_Str;
    }

    private Room.CurrentRoundState _activeRoundState = Room.CurrentRoundState.Answer;
    public Room.CurrentRoundState activeRoundState
    {
        get
        {
            return _activeRoundState;
        }
        set
        {
            _activeRoundState = value;
        }
    }

    private Room room;
    [SerializeField] private Room.Status status;
    [SerializeField] private string currentGameId;
    [SerializeField] private TextMeshProUGUI roomNameField;
    [SerializeField] private TextMeshProUGUI leaderNameField;
    [SerializeField] private TextMeshProUGUI winnerNameField;
    [SerializeField] private TextMeshProUGUI leaderScoreField;
    [SerializeField] private TextMeshProUGUI leaderPointsTag;
    [SerializeField] private TextMeshProUGUI roundNumberField;
    [SerializeField] private TextMeshProUGUI playerIconTrayText;
    [SerializeField] private Transform playerIconTray;
    [SerializeField] private PlayerIcon[] playerIcons;
    [SerializeField] private TextMeshProUGUI stagingPlayersText;
    [SerializeField] private Transform roundBadge;
    [SerializeField] private Transform leaderWidget;
    [SerializeField] private Transform winnerWidget;
    [SerializeField] private Transform winnerTrophy;
    [SerializeField] private Transform avatar;
    [SerializeField] private Avatar leaderGravatar;
    [SerializeField] private Avatar winnerGravatar;
    [SerializeField] private Transform ownerWidget;
    [SerializeField] private Transform qAndAWidget;
    [SerializeField] private Transform latestSubmissionButton;
    [SerializeField] private Transform votingWidget;
    [SerializeField] private Transform stagingWidget;
    [SerializeField] private Transform openingRoundWidget;
    [SerializeField] private TextMeshProUGUI openingRoundText;
    [SerializeField] private TextMeshProUGUI ownerNameField;
    [SerializeField] private TextMeshProUGUI pinValue;
    [SerializeField] private ChatButton chatButton;
    [SerializeField] private Image largeCheck;

    [SerializeField] private TextMeshProUGUI modifiedDate;
    [SerializeField] private Transform roundSummaryEnvelopeWidget;
    [SerializeField] private TextMeshProUGUI envelopeRoundNumber;
    [SerializeField] private Transform roundSummaryEnvelopeMainText;
    [System.NonSerialized] public GameLobby.BinType binType;

    [SerializeField] private Animator actionRequiredHighlight;

    private string leaderEmail;
    private string winnerEmail;

    private Transform selectedEnvelopeWidget;

    void Awake()
    {
        anim = this.gameObject.GetComponent<Animator>();
    }

    private bool returnSuccess = false;
    void StateChangeCompleteListener(UIStateManager.StateNames newState, UIStateManager.StateNames currentState, UIStateChange.TransitionType type)
    {
        StartCoroutine(ShowCheckRoutine(newState));

        if( currentState == UIStateManager.StateNames.GameLobby )
        {
            if( selectedEnvelopeWidget != null )
            {
                selectedEnvelopeWidget.gameObject.SetActive(false);
                selectedEnvelopeWidget = null;
            }
        }
    }

    private IEnumerator ShowCheckRoutine(UIStateManager.StateNames newState)
    {
        if (currentGame != null)
        {
            if (newState == UIStateManager.StateNames.GameLobby)
            {
                if (GameLobby.lastSuccessPlay == currentGame.id || GameLobby.lastSuccessVote == currentGame.id)
                {
                    returnSuccess = true;
                    yield return new WaitForSeconds(0.5f);
                    largeCheck.GetComponent<Animator>().SetTrigger("ShowCheck");
                    SoundManager.SoundElementNames element = GameLobby.lastSuccessVote == "" ? SoundManager.SoundElementNames.ActionComplete1 : SoundManager.SoundElementNames.ActionComplete2;
                    SoundManager.instance.Play(element);

                    if (GameLobby.lastSuccessPlay == currentGame.id && activeRoundState == Room.CurrentRoundState.Voting)
                    {
                        Globals.MemeLog(MethodBase.GetCurrentMethod(), "Last vote was played. Show alert popup for voting ready.");
                        GameBecameReadyForVoting?.Invoke(room);
                    }

                    if (DataManager.ShowAds(AdsManager.AdType.Interstitial))
                    {
                        yield return new WaitForSeconds(1.25f);
                        ShowInterstitialAd?.Invoke();
                    }       
                }
                GameLobby.lastSuccessPlay = "";
                GameLobby.lastSuccessVote = "";
            }
        }
    }

    public void SetGameBlockActive(bool active)
    {
        string inactiveName = "Block_Inactive";
        this.gameObject.SetActive(active);
        if (room != null)
            this.gameObject.name = active ? "Block_" + room.displayName : inactiveName;
        else
            this.gameObject.name = inactiveName;
    }

    public void SetupBlock(Room inRoom, string newRoomName, GameLobby.BinType bin)
    {
        // TESTING
        //inRoom.showRoundSummary = 1;

        leaderEmail = "";
        winnerEmail = "";

        binType = bin;

        bool thisBlockShowsActionComplete = false;
        currentGame = DataManager.GetCurrentGame(inRoom);
        inRoom.status = DataManager.GetRoomStatus(inRoom);

        bool hasWinner = DataManager.GameHasWinner(currentGame);
        bool wasInGame = DataManager.IsPlayerInGame(Globals.email, currentGame);

        bool gameAfterWasDeleted = DataManager.NewestGameWasDeleted(inRoom, currentGame);
        if (hasWinner && gameAfterWasDeleted)
        {
            Debug.Log("FORCING hasWinner TO FALSE FOR ROOM " + inRoom.displayName);
            hasWinner = false;
        }

        bool currentGameCompletePastDue = false;

        long stasisStartTime = DataManager.GetStasisLaunchTime(inRoom);

        stasisStartInfo = null;
        if( stasisStartTime > 0 )
            stasisStartInfo = DataManager.GetTimeUntilStasisStart(stasisStartTime);

        bool summary = false;
        if (currentGame != null)
        {
            currentGameId = currentGame.id;
            currentGameCompletePastDue = DataManager.GetGameCompleteButPastDue(currentGame, inRoom);

            int mod = Mathf.Abs( (int)currentGame.modified);
            summary = inRoom.showRoundSummary > 0 || (hasWinner && inRoom.showRoundSummary >= 0);

            if (gameAfterWasDeleted)
                summary = false;
            if (inRoom.status == Room.Status.Staging)
                summary = false;

            roundSummaryEnvelopeWidget.gameObject.SetActive(summary);
            roundSummaryEnvelopeMainText.gameObject.SetActive(summary);
            if (summary)
            {
                int summaryValue = DataManager.GameHasWinner(currentGame) ? inRoom.showRoundSummary + 1 : inRoom.showRoundSummary;
                envelopeRoundNumber.text = summaryValue.ToString();
                inRoom.showRoundSummary = summaryValue;
            }
        }
        else
        {
            roundSummaryEnvelopeWidget.gameObject.SetActive(false);
            winnerTrophy.gameObject.SetActive(false);
            winnerWidget.gameObject.SetActive(false);
        }

        if (DataManager.GameHasWinner(currentGame) && DataManager.NewestGameWasDeleted(inRoom, currentGame) && stasisStartInfo == null )
        {
            inRoom.status = Room.Status.Staging;
            Debug.Log("stagingSet2");
        }
            

        room = inRoom;
        status = room.status;
        if (room.displayName != "")
            roomNameField.text = room.displayName;
        else
            roomNameField.text = room.name;

        if (room.status == Room.Status.Staging)
        {
            pinValue.text = room.pin;
            votingWidget.gameObject.SetActive(false);
            qAndAWidget.gameObject.SetActive(false);
            latestSubmissionButton.gameObject.SetActive(false);
            stagingPlayersText.text = "Players in " + DataManager.GetRoomNameString() + ": " + (room.roomPlayers.Count).ToString();

            if (inRoom.owner.email == Globals.email)
                modifiedDate.text = "Setup Game";
            else
                modifiedDate.text = "Wait for Start";
            if (newRoomName != null)
            {
                if (newRoomName.ToLower() == room.displayName.ToLower())
                {
                    this.gameObject.SetActive(true);
                    StartCoroutine(StorageUpdate());
                }
            }
        }
        else
            SetupStatusField(inRoom, currentGame, stasisStartInfo);

        lastWrite = inRoom.lastWrite;
        currentTimeOutput = modifiedDate.text;

        ownerNameField.text = inRoom.owner.displayName;
        ownerWidget.gameObject.SetActive(inRoom.games.Count == 0 || currentGameCompletePastDue || room.status == Room.Status.Staging);

        roundBadge.gameObject.SetActive(inRoom.status == Room.Status.Active);

        stagingPlayersText.gameObject.SetActive(room.status == Room.Status.Staging);

        if (inRoom.games.Count != 0)
        {
            RESTInterface.GameSummaryOut game = currentGame;
            activeRoundState = DataManager.GetRoundState(game);

            bool showLeaderWidget = game.round > 1 && !DataManager.GameHasWinner(game);
            leaderWidget.gameObject.SetActive(showLeaderWidget);
            if (showLeaderWidget)
            {
                List<DataManager.Leader> leader = DataManager.GetLeader(game, DataManager.GetRoundState(game));
                if (leader.Count > 0)
                {
                    leaderScoreField.text = (leader[0].score).ToString();
                    leaderPointsTag.text = leader[0].score > 1 ? "pts" : "pt";
                    if (leader.Count == 1)
                    {
                        leaderNameField.text = leader[0].player.userOut.displayName;
                    }
                    else
                        leaderNameField.text = leader.Count.ToString() + " Tied";
                    SetLeaderAvatar(leader);
                }
                else
                    print("Leader count for " + inRoom.displayName + ": " + leader.Count);
            }

            openingRoundWidget.gameObject.SetActive(game.round == 1 && !DataManager.GameHasWinner(game));

            RESTInterface.RoundSummaryOut currentRound = null;

            currentRound = DataManager.GetCurrentRound(inRoom);
            openingRoundText.text = "Opening Round!";
            roundNumberField.text = game.round.ToString();
            votingWidget.gameObject.SetActive(activeRoundState == Room.CurrentRoundState.Voting);

            bool showQAWidget = activeRoundState == Room.CurrentRoundState.Answer;
            bool showLatestSubmission = false;
            if( showQAWidget & showMySubmission )
            {
                if( binType == GameLobby.BinType.Waiting )
                {
                    showQAWidget = false;
                    showLatestSubmission = true;
                }
            } 
            qAndAWidget.gameObject.SetActive(showQAWidget);
            latestSubmissionButton.gameObject.SetActive(showLatestSubmission);

            stagingWidget.gameObject.SetActive(room.status == Room.Status.Staging);

            if (activeRoundState == Room.CurrentRoundState.Voting)
            {
                latestSubmissionButton.gameObject.SetActive(false);
                playerIconTrayText.text = "Voted:";
                int numVotes = 0;
                if (currentRound.votes != null)
                {
                    numVotes = currentRound.votes.Length;
                }

                if (numVotes == 0 && thisBlockShowsActionComplete)
                {
                    thisBlockShowsActionComplete = false;
                    GameLobby.actionCompleted = "";
                }
                if (game.round == 1)
                {
                    leaderWidget.gameObject.SetActive(false);
                    openingRoundText.text = "Voting Begins!";
                    openingRoundWidget.gameObject.SetActive(true);
                }
            }
            else if (activeRoundState == Room.CurrentRoundState.Answer)
                playerIconTrayText.text = "Played:";

            if (!thisBlockShowsActionComplete)
            {
                bool doUpdate = false;
                TimestampStorage storage = PlayerPrefsInterface.GetStoredTimestamp(currentGame);
                if (storage == null)
                {
                    doUpdate = true;
                }
                else
                {
                    long storedTimestamp = Convert.ToInt64(storage.timestamp);
                    if (inRoom.lastWrite > storedTimestamp)
                    {
                        Globals.MemeLog(MethodBase.GetCurrentMethod(), "lastWrite is greater than storedTimestamp " + Time.time);
                        doUpdate = true;
                    }
                }
                if (doUpdate)
                {
                    this.gameObject.SetActive(true);
                    StartCoroutine(StorageUpdate());
                }
            }
        }
        else
        {
            stagingWidget.gameObject.SetActive(true);
            leaderWidget.gameObject.SetActive(false);
            openingRoundWidget.gameObject.SetActive(false);
        }

        SetupChatButton(inRoom);

        //Debug.Log(room.displayName + " currentGame == null:" + (currentGame == null) + " currentGameCompletePastDue: " + currentGameCompletePastDue);
        if (currentGame != null && !currentGameCompletePastDue)
        {
            //Debug.Log(room.displayName + " hasWinner: " + hasWinner + " status: " + room.status.ToString());
            playerIconTray.gameObject.SetActive(!hasWinner && room.status != Room.Status.Staging);
            //Debug.Log(room.displayName + " summary: " + summary);
            winnerWidget.gameObject.SetActive(hasWinner && !summary);
            winnerTrophy.gameObject.SetActive(hasWinner);

            if (!hasWinner)
                SetupPlayerIcons(thisBlockShowsActionComplete);
            else
            {
                RESTInterface.UserSummaryOut winner = currentGame.winner.userOut;
                winnerNameField.text = winner.displayName;
                SetWinnerAvatar(winner);
            }
        }
        else
            playerIconTray.gameObject.SetActive(false);
    }

    private void SetupStatusField(Room inRoom, RESTInterface.GameSummaryOut game = null, GameBlock.StasisStartInfo stasisStartInfo = null)
    {
        switch (DataManager.GetRoundState(inRoom))
        {
            case Room.CurrentRoundState.Answer:
                modifiedDate.text = DataManager.GetTimeUntilEnd(inRoom.questionStarted, game, Room.CurrentRoundState.Answer);
                break;
            case Room.CurrentRoundState.Voting:
                modifiedDate.text = DataManager.GetTimeUntilEnd(inRoom.voteStarted, game, Room.CurrentRoundState.Voting);
                break;
            case Room.CurrentRoundState.Complete:
                {
                    if (DataManager.GameHasWinner(game))
                    {
                        string value = "Game is over.";
                        string stasisInfo = "";
                        if (stasisStartInfo != null)
                            stasisInfo = " " + DataManager.GetStasisInfoText(stasisStartInfo);
                        modifiedDate.text = value + stasisInfo;
                    }   
                    else
                        modifiedDate.text = "Round is over.";
                }
                break;
        }
    }

    private void SetupChatButton(Room inRoom)
    {
        if (inRoom.chatEntries == null)
            SetChatBadge(0);
        else if (inRoom.chatEntries.Length == 0)
            SetChatBadge(0);
        else
        {
            int chatBadgeAmount = DataManager.GetChatBadgeAmount(inRoom);
            SetChatBadge(chatBadgeAmount);
        }
    }

    private void SetChatBadge(int numToSee)
    {
        chatButton.Set(room, numToSee);
    }

    private void SetLeaderAvatar(List<DataManager.Leader> leaders)
    {
        avatar.gameObject.SetActive(leaders.Count == 1);
        if (leaders.Count == 1)
        {
            leaderEmail = leaders[0].player.userOut.email;
            leaderGravatar.Set(leaders[0].player.userOut);
        }
        else
            leaderEmail = "";
    }

    private void SetWinnerAvatar(RESTInterface.UserSummaryOut winner)
    {
        winnerGravatar.Set(winner);
    }

    private IEnumerator StorageUpdate(bool playAudio = true)
    {
        if (currentGame != null)
        {
            Globals.MemeLog(MethodBase.GetCurrentMethod(), "STORAGE UPDATE! " + currentGame.id);

            TimestampStorage storage = new TimestampStorage();
            storage.timestamp = lastWrite.ToString();
            storage.currentRoundState_Str = activeRoundState.ToString();
            PlayerPrefsInterface.SaveStorageUpdate(currentGame, storage);
        }
        else
            Globals.MemeLog(MethodBase.GetCurrentMethod(), "STORAGE UPDATE, NO GAME, JUST NEW ROOM.");

        if (playAudio)
            SoundManager.instance.Play(SoundManager.SoundElementNames.RefreshUpdate1, 0.5f);

        yield return new WaitForSeconds(0.5f);
        anim.SetTrigger("Update");
        modifiedDate.text = "UPDATE!";
        yield return new WaitForSeconds(2.5f);
        modifiedDate.text = currentTimeOutput;
    }

    private void SetupPlayerIcons(bool showActionComplete = false, bool forcedShowSummary = false)
    {
        // ROOMPLAYERS DOES NOT INCLUDE THE OWNER
        int playerCount = room.roomPlayers.Count + 1;

        bool hasGame = DataManager.GetRoomHasGame(room);
        RESTInterface.GameSummaryOut cGame = null;
        int numVoted = 0;
        if (hasGame)
        {
            cGame = DataManager.GetCurrentGame(room);
            playerCount = cGame.players.Length;

            if (cGame.currentRound.votes == null)
                return;

            numVoted = cGame.currentRound.votes.Length;
        }
        for (int i = 0; i < playerIcons.Length; i++)
        {
            playerIcons[i].transform.parent.gameObject.SetActive(i < playerCount);
            if (i < playerCount)
            {
                if (hasGame && !forcedShowSummary && !DataManager.GameHasWinner(cGame))
                {
                    if (activeRoundState == Room.CurrentRoundState.Answer)
                    {
                        int numAnswered = cGame.currentRound.answers.Length;
                        playerIcons[i].playerIconMask.enabled = i < numAnswered;
                        if (numAnswered - 1 == i && showActionComplete)
                            PlayActionCompleteAnim(playerIcons[i], activeRoundState);
                    }
                    else
                        playerIcons[i].playerIconMask.enabled = i < numVoted;
                }
                else
                    playerIcons[i].playerIconMask.enabled = false;
            }
        }
        if (activeRoundState == Room.CurrentRoundState.Voting)
        {
            for (int j = 0; j < playerCount; j++)
                playerIcons[j].playerIconMask.enabled = j < numVoted || forcedShowSummary;
            if (showActionComplete && !forcedShowSummary)
                PlayActionCompleteAnim(playerIcons[numVoted - 1], activeRoundState);
        }
    }

    private RESTInterface.GameSummaryOut currentGame;
    private long lastWrite;
    private string currentTimeOutput;

    private void PlayActionCompleteAnim(PlayerIcon playerIcon, Room.CurrentRoundState currentRoundState)
    {
        playerIcon.PlayActionComplete();
        GameLobby.actionCompleted = "";
        /*
        float audioChimeDelay = 0.5f;
        if (currentRoundState == Room.CurrentRoundState.Answer)
            SoundManager.instance.Play(SoundManager.SoundElementNames.ActionComplete1, audioChimeDelay);
        else
            SoundManager.instance.Play(SoundManager.SoundElementNames.ActionComplete2, audioChimeDelay);
        */

        this.gameObject.SetActive(true);
        StartCoroutine(StorageUpdate(false));
    }

#if UNITY_EDITOR
    bool testWinnerScreen = false;
#endif

    public void SelectGame()
    {
        // QUESTION 1 - IS THERE A GAME WE NEED TO CARE ABOUT?
        if ( DataManager.GetRoomHasGame(room) )
        {

#if UNITY_EDITOR
            if( testWinnerScreen )
            {
                TestWinnerScenario();
                Debug.Log("Test Winner Scenario.");
                return;
            }
#endif

            Leaderboard.lastGameBlockClicked = this;
            RESTInterface.GameSummaryOut gameSummaryOut = DataManager.GetCurrentGame(room);

            bool isStaging = room.status == Room.Status.Staging;
            if (isStaging)
            {
                BroadcastRoom?.Invoke(room);
                return;
            }

            // QUESTION 2 - IS THERE A SUMMARY WE NEED TO DO?
            bool doRoundSummary = room.showRoundSummary > 0;
            //Debug.Log("room.showRoundSummary: " + room.showRoundSummary);

            bool inCurrentGame = DataManager.IsPlayerInGame(Globals.email, gameSummaryOut);
            if (doRoundSummary)
            {
                if (roundSummaryEnvelopeWidget.gameObject.activeInHierarchy)
                    selectedEnvelopeWidget = roundSummaryEnvelopeWidget;

                DataManager.SetCoreFlow(DataManager.CoreFlow.ShowASummary);

                RoundSummary.SummaryState summaryState = new RoundSummary.SummaryState();
                summaryState.summaryRound = room.showRoundSummary;

                summaryState.binType = binType;
  
                BroadcastSummaryInfo?.Invoke(summaryState);
            }
            else
                DataManager.SetCoreFlow(DataManager.CoreFlow.Standard);

            StartCoroutine(RESTInterface.instance.GetGameDetail(gameSummaryOut.id, false, room));
        }
        else
            DataManager.SetCoreFlow(DataManager.CoreFlow.Standard);

        // THE CHANNEL WILL ALWAYS PICK THIS UP AND CONFIGURE IT
        BroadcastRoom?.Invoke(room);
    }

    private void TestWinnerScenario()
    {
        RESTInterface.GameSummaryOut game = DataManager.GetCurrentGame(room);
        StartCoroutine(RESTInterface.instance.GetGameDetail(game.id, false, room, UIStateManager.StateNames.Winner));
    }

    void RetrievedGameDetailListener(RESTInterface.GameDetailOut game, bool toGameRound, Room returnRoom, UIStateManager.StateNames state)
    {
        if (state != UIStateManager.StateNames.Winner)
            return;
        if (room != returnRoom)
            return;

        Debug.Log("Test Winner Listener.");

        List<RESTInterface.PlayerOut>[] playerPockets = new List<RESTInterface.PlayerOut>[4];
        for( int i = 0; i < game.players.Length; i++ )
        {
            if (playerPockets[i] == null)
                playerPockets[i] = new List<RESTInterface.PlayerOut>();

            if (i >= 3)
                playerPockets[3].Add(game.players[i]);
            else
                playerPockets[i].Add(game.players[i]);
        }
        LoadingCircle.instance.Toggle(false);
        TestWinnerScenarioEvent?.Invoke(playerPockets);
    }

    public void PulseForActionRequired(int counter)
    {
        if (LoadingCircle.instance != null)
        {
            if (LoadingCircle.instance.GetOpenJobs() <= 0)
                actionRequiredHighlight.SetTrigger("Pulse");
        }       
    }
}
