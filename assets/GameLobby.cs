﻿// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// THE GUTS OF THE GAMELOBBY SCENE ARE HERE, GAME BLOCKS ARE SORTED TO BINS,
// 1. ACTION REQUIRED 2. WAITING 3. COMPLETED
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class GameLobby : UIState {

    public delegate void EditorDebugLogoutDelegate();
    public static event EditorDebugLogoutDelegate EditorDebugLogout;

    public static GameLobby instance;

    [SerializeField]
    private Transform contentContainer;

    // FOR THE 3 BINS
    private List<Room>[] gameRoomLists = new List<Room>[(int)BinType.NUM_OF_BIN_TYPES];

    private List<Room> allGameRooms;

    public static string actionCompleted = "";

    [SerializeField] private GameObject welcomeBlock;
    [SerializeField] private Button createButton;
    [SerializeField] private GameObject currencyWidget;

    [SerializeField] private GameObject nativeAdWidgetSource;
    private GameObject nativeAdWidget_Active;

    public enum BinType
    {
        ActionRequired,
        Waiting,
        Completed,
        Staging,
        SittingOut,
        NUM_OF_BIN_TYPES
    }
    [SerializeField]
    private Bin rootBin;
    private List<Bin> bins;
    public static int numActiveBins;

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // EVENT LISTENERS
    new void OnEnable () { RegisterStates(true); }
    new void OnDisable () { RegisterStates(false); }
    new void OnDestroy () { RegisterStates(false); }
    private void RegisterStates (bool register)
    {
        if (register)
        {
            RESTInterface.DispatchRooms += DispatchRoomsListener;
            UIStateManager.StateChangeEvent += StateChangeListener;
            UIStateManager.AppRefocus += LobbyRefocusListener;
            RESTInterface.PlayCardSuccess += PlayCardSuccessListener;
            RESTInterface.VoteCardSuccess += VoteCardSuccessListener;
            NativeAdWidget.PositionNativeAd += PositionNativeAdListener;
            ScrollRefresh.ScrollRefreshEvent += ScrollRefreshListener;
            PopupBasic.BasicPopupPress += BasicPopupListener;
            RESTInterface.FullStopCoroutinesEvent += FullStopCoroutineListener;
        }
        else {
            RESTInterface.DispatchRooms -= DispatchRoomsListener;
            UIStateManager.StateChangeEvent -= StateChangeListener;
            UIStateManager.AppRefocus -= LobbyRefocusListener;
            RESTInterface.PlayCardSuccess -= PlayCardSuccessListener;
            RESTInterface.VoteCardSuccess -= VoteCardSuccessListener;
            NativeAdWidget.PositionNativeAd -= PositionNativeAdListener;
            ScrollRefresh.ScrollRefreshEvent -= ScrollRefreshListener;
            PopupBasic.BasicPopupPress -= BasicPopupListener;
        }
    }
    //::::::::::::::::::::::::::::::::::::::::::::::::::::::

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // COROUTINE MANAGEMENT
    Coroutine GetGameRoomsCR;
    void FullStopCoroutineListener()
    {
        if(GetGameRoomsCR != null)
            StopCoroutine(GetGameRoomsCR);
    }
    //::::::::::::::::::::::::::::::::::::::::::::::::::::::

    new void StateChangeListener(UIStateManager.StateNames newState, UIStateManager.StateNames currentState, UIStateChange.TransitionType type)
    {
        //Debug.Log("uiState: " + uiState.ToString() + " currentState: " + currentState.ToString() + " newState: " + newState.ToString());
        if( newState == uiState )
        {
            string testTimespan = DataManager.ReportThisTimespan();

            base.StateChangeListener(newState, currentState, type);

            if (DataManager.ShowAd(AdsManager.AdType.Native, UIStateManager.StateNames.GameLobby))
                HandleNativeAd();
            else
                AdsManager.instance.SuppressNativeAd(uiState);

            if (currencyWidget != null)
                currencyWidget.SetActive(Globals.CurrencyAvailableForGame());

            bool initialJoinPopupShown = CloudManager.instance.GetCloudInitialJoinPopup();
            if (!initialJoinPopupShown)
                StartCoroutine(ShowJoinPopRoutine());
        }
    }

    private IEnumerator ShowJoinPopRoutine()
    {
        yield return new WaitForSeconds(2.5f);
        Popup.instance.Spawn(Popup.PopupListing.Basic, PopupBasic.BasicType.InitialJoinPopup);
        CloudManager.instance.SetInitialPopupShown();
    }

    void BasicPopupListener(PopupBasic.BasicType basicType, PopupBasic.ButtonNum buttonNum)
    {
        if( basicType == PopupBasic.BasicType.InitialJoinPopup )
        {
            if (buttonNum == PopupBasic.ButtonNum.Top)
                UIStateManager.instance.ChangeState(UIStateManager.StateNames.FindGame, UIStateChange.TransitionType.Forward, true, false);
            else
                UIStateManager.instance.ChangeState(UIStateManager.StateNames.CreateGame, UIStateChange.TransitionType.Forward, true, false);
        }
    }

    private void HandleNativeAd()
    {
        if( nativeAdWidget_Active != null )
            Destroy(nativeAdWidget_Active);

        nativeAdWidget_Active = Instantiate(nativeAdWidgetSource, nativeAdWidgetSource.transform.parent) as GameObject;
        nativeAdWidgetSource.SetActive(false);
        nativeAdWidget_Active.SetActive(true);
        NativeAdWidget naW = nativeAdWidget_Active.GetComponent<NativeAdWidget>();
        if (naW != null)
            naW.SetActiveForUse();

        AdsManager.instance.RequestNativeAd(uiState);
    }

    new void Awake ()
    {
        InitBinList ();

        instance = this;

        CleanUpCameras ();

        if (UIStateManager.instance == null)
        {
            SpawnStateManager ();
        } else
        {
            StartCoroutine (MoveOffScreen (UIStateManager.instance.gameStartingState));
        }

        localCanvas = this.gameObject.GetComponent<Canvas>();
        if (localCanvas != null)
        {
            if (localCanvas.gameObject.GetComponent<CanvasHelper>() == null)
                localCanvas.gameObject.AddComponent<CanvasHelper>();
        }
    }

    new void Start ()
    {
        UIStateManager.instance.RegisterState(this);
        StartCoroutine(InitBins());
    }

    public static string lastSuccessPlay = "";
    public static string lastSuccessVote = "";
    void PlayCardSuccessListener(string gameId)
    {
        lastSuccessPlay = gameId;
    }

    void VoteCardSuccessListener(string gameId)
    {
        lastSuccessVote = gameId;
    }

    private void InitBinList ()
    {
        bins = new List<Bin>();
    }

    private IEnumerator InitBins ()
    {
        for (int i = 0; i < (int)BinType.NUM_OF_BIN_TYPES; i++)
        {
            Bin bin;
            GameObject binObj;

            if (i == 0)
            {
                bin = rootBin;
                binObj = rootBin.gameObject;
                rootBin.InitGameBlockPool(Bin.gameBlockPoolSizePerBin);
            } else
            {    
                yield return null;
                binObj = Instantiate (rootBin.gameObject, rootBin.transform.position, Quaternion.identity) as GameObject;
                binObj.transform.SetParent (contentContainer);
                binObj.transform.localScale = Vector3.one;
                bin = binObj.GetComponent<Bin> ();
                Destroy(bin.NativeAdObject);
            }
            bins.Add(bin);
            binObj.name = "Bin_" + ((BinType)i).ToString ();
            bin.BinType = (BinType)i;
        }
    }

    private IEnumerator SortRoomsToBins (List<Room> rooms, string newRoomName)
    {
        HandleOffScreenObjects(true);

        for (int n = 0; n < (int)BinType.NUM_OF_BIN_TYPES; n++)
        {
            if (gameRoomLists [n] == null)
                gameRoomLists [n] = new List<Room> ();
            gameRoomLists [n].Clear ();
        }

        while (bins.Count < (int)BinType.NUM_OF_BIN_TYPES)
            yield return null;

        for (int i = 0; i < rooms.Count; i++)
        {
            Room thisRoom = rooms [i];
            string owner = rooms [i].owner.email;

            DebugEditorTestLog(thisRoom, "DataManager.GetRoomHasGame", (DataManager.GetRoomHasGame(thisRoom)).ToString());

            if (!DataManager.GetRoomHasGame (thisRoom))
                AddToBin(BinType.Staging, thisRoom);
            else
            {
                RESTInterface.GameSummaryOut thisGame = DataManager.GetCurrentGame (thisRoom);
                Room.CurrentRoundState roundState = DataManager.GetRoundState (thisGame);
                BinType bin = BinType.ActionRequired;
                bool isPlayerInGame = DataManager.IsPlayerInGame(Globals.email, thisGame);

                DebugEditorTestLog(thisRoom, "Status", thisRoom.status.ToString());

                if (thisRoom.status == Room.Status.Complete)
                {
                    if (DataManager.NewestGameWasDeleted(thisRoom, thisGame))
                        bin = BinType.Staging;
                    else
                        bin = BinType.Completed;
                }            
                else
                {
                    if (roundState == Room.CurrentRoundState.Answer)
                    {
                        if (isPlayerInGame)
                        {
                            bool answered = DataManager.GetRoundAnswered(thisGame.currentRound);
                            bin = answered ? BinType.Waiting : BinType.ActionRequired;
                        }
                        else
                            bin = BinType.SittingOut;
                    }
                    else if (roundState == Room.CurrentRoundState.Voting)
                    {
                        if (isPlayerInGame)
                        {
                            bool voted = DataManager.GetRoundVoted(thisGame.currentRound, Globals.email);
                            bin = voted ? BinType.Waiting : BinType.ActionRequired;
                        }
                        else
                            bin = BinType.SittingOut;

                        if (isPlayerInGame && DataManager.GetAllVotesIn(thisGame.currentRound, thisGame.players.Length))
                            bin = BinType.ActionRequired;
                    }
                }

                // POSSIBLY OVERRIDE THE CALCULATED BIN BECAUSE A SUMMARY NEEDS TO BE SHOWN STILL
                if ((thisGame.round > 1 || DataManager.GameHasWinner(thisGame)) && isPlayerInGame && !DataManager.NewestGameWasDeleted(thisRoom, thisGame))
                {
                    Room.CurrentRoundState state = DataManager.GetRoundState(thisGame);
                    int roundCheck = roundState == Room.CurrentRoundState.Complete ? thisGame.round : thisGame.round - 1;

                    if (PlayerPrefsInterface.GetLastRoundSummarySeen(thisGame) < roundCheck)
                    {
                        bool alreadyWent = false;
                        if (state == Room.CurrentRoundState.Answer)
                        {
                            bool hasPlayed = DataManager.GetRoundAnswered(thisGame.currentRound);
                            if (hasPlayed)
                                alreadyWent = true;
                        }
                        else if (state == Room.CurrentRoundState.Voting)
                        {
                            bool hasVoted = DataManager.GetRoundVoted(thisGame.currentRound, Globals.email);
                            if (hasVoted)
                                alreadyWent = true;
                        }
                        if (!alreadyWent)
                        {
                            int showRoundSummaryValue = thisGame.round - 1;
                            thisRoom.showRoundSummary = showRoundSummaryValue;
                        }
                        else
                            thisRoom.showRoundSummary = -1;
                    }
                    else
                        thisRoom.showRoundSummary = -1;

                    if (thisRoom.showRoundSummary > 0)
                        bin = BinType.ActionRequired;
                }
                AddToBin (bin, thisRoom);
            }
        }
        for (int l = 0; l < (int)BinType.NUM_OF_BIN_TYPES; l++)
        {
            bool binActive = gameRoomLists [l].Count > 0;
            bins [l].gameObject.SetActive (binActive);
            if (gameRoomLists [l].Count > 1)
            {
                if (l == (int)BinType.Staging)
                {
                    gameRoomLists[l] = SortBinAlphabetically(gameRoomLists[l]);
                }
                else
                    gameRoomLists [l] = SortBinForDate (gameRoomLists [l]);
            }
            for (int m = 0; m < Bin.gameBlockPoolSizePerBin; m++)
            {
                if( m > gameRoomLists[l].Count-1 )
                    bins[l].gameBlocks[m].SetGameBlockActive(false);
            }
            SetupBlocks ((BinType)l, newRoomName);
        }
        SetNumActiveBins();
        UpdateContentWindow();
    }

    private List<Room> SortBinForDate(List<Room> binList)
    {
        IComparer<Room> sorter = new sortBinBasedOnDateEpoch();
        binList.Sort(sorter);
        return binList;
    }

    private List<Room> SortBinAlphabetically(List<Room> binList)
    {
        IComparer<Room> sorter = new sortBinBasedOnAlphabetic();
        binList.Sort(sorter);
        return binList;
    }

    public class sortBinBasedOnAlphabetic : IComparer <Room>
    {
        public int Compare (Room a, Room b)
        {
            return string.Compare(a.name, b.name);
        }
    }

    public class sortBinBasedOnDateEpoch : IComparer<Room>
    {
        public int Compare (Room a, Room b)
        {
            if (a.lastWrite < b.lastWrite )
                return 1;
            else if (a.lastWrite > b.lastWrite )
                return -1;
            else
                return 0;
        }
    }

    private void DebugEditorTestLog(Room room, string str, string output)
    {
#if UNITY_EDITOR
        if( Presentation.instance.editorGroupTestLog != "" )
        {
            if (room.name.ToLower() == Presentation.instance.editorGroupTestLog)
                Debug.Log("DebugEditorTestLog, " + str + ": " + output);
        }
#endif
    }

    private void AddToBin (BinType bin, Room room)
    {
        int binIndex = (int)bin;
        gameRoomLists [binIndex].Add (room);
    }

    private void SetupBlocks (BinType bin, string newRoomName)
    {
        int binIndex = (int)bin;
        List<Room> gameRoomList = gameRoomLists [binIndex];

        for (int i = 0; i < gameRoomList.Count; i++)
        { 
            bins [binIndex].gameBlocks [i].SetupBlock (gameRoomList [i], newRoomName, bin);
            bins[binIndex].gameBlocks[i].SetGameBlockActive(true);
        }
    }

    private void UpdateContentWindow()
    {
        contentContainer.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 1f);
        UIStateManager.instance.ChangeState(UIStateManager.StateNames.GameLobby);
    }

    void DispatchRoomsListener (RESTInterface.GameRoomOut[] rooms, string newRoomName)
    {
        createButton.interactable = !Globals.createRoomLocked || DataManager.IsOnAdminList(Globals.email) || Application.isEditor;

        if (UIStateManager.instance.GetCurrentState() != UIStateManager.StateNames.GameLobby)
            UIStateManager.instance.ChangeState(UIStateManager.StateNames.GameLobby, UIStateChange.TransitionType.Forward, true, false); // !fromLogin);
        allGameRooms = new List<Room>();
        allGameRooms = DataManager.RetrieveGameRooms(rooms);
        StartCoroutine(SortRoomsToBins(allGameRooms, newRoomName));

        welcomeBlock.SetActive(allGameRooms.Count == 0);
    }

    public void ClickCreateGame ()
    {
        UIStateManager.instance.ChangeState(UIStateManager.StateNames.CreateGame, UIStateChange.TransitionType.Forward, true, true);
    }

    public void ClickJoinGame()
    {
        UIStateManager.instance.ChangeState(UIStateManager.StateNames.FindGame, UIStateChange.TransitionType.Forward, true, true);
    }

    void LobbyRefocusListener(UIStateManager.StateNames state)
    {
        if( state == uiState)
            ClickRefresh(true);
    }

    public void ClickRefresh()
    {
        ClickRefresh(false);
    }

    public void ClickRefresh (bool force)
    {
        LoadingCircle.instance.Toggle (true, "Click Refresh");

        if (Globals.OKToGameRoomQuery(force))
            GetGameRoomsCR = StartCoroutine(RESTInterface.instance.GetGameRooms (null, true));
        else
            StartCoroutine(SimulateLoad());
    }

    private IEnumerator SimulateLoad ()
    {
        Globals.MemeLog(MethodBase.GetCurrentMethod(), "Simulate Load " + Time.time);
        RESTInterface.instance.SendToCallLog(RESTInterface.Calls.GetGameRooms, RESTInterface.CallTrigger.Start);
        yield return new WaitForSeconds (0.75f);
        RESTInterface.instance.SendToCallLog(RESTInterface.Calls.GetGameRooms, RESTInterface.CallTrigger.End);
        LoadingCircle.instance.Toggle (false, "Simulate Load");
    }

    void ChatPostedListener(string roomId)
    {
        if( UIStateManager.instance.GetCurrentState() == UIStateManager.StateNames.GameLobby )
        {
            GetGameRoomsCR = StartCoroutine(RESTInterface.instance.GetGameRooms(null));
        }   
    }

    void ScrollRefreshListener(UIStateManager.StateNames state)
    {
        if (state != uiState)
            return;

        ClickRefresh(false);
    }

    private void SetNumActiveBins()
    {
        int count = 0;
        for (int i = 0; i < bins.Count; i++)
        {
            if (bins[i].gameObject.activeInHierarchy)
                count++;
        }
        numActiveBins = count;
    }

    public static int GetNumActiveBins()
    {
        return numActiveBins;
    }

    void PositionNativeAdListener(NativeAdWidget nativeAdWidget)
    {
        if (nativeAdWidget.state != uiState)
            return;

        int random = UnityEngine.Random.Range(0, numActiveBins);

        int counter = 0;
        int binIndex = 0;
        for( int i = 0; i < bins.Count; i++ )
        {
            if( bins[i].gameObject.activeInHierarchy )
            {
                if (counter < random)
                    counter++;
                else
                {
                    binIndex = i;
                    break;
                }        
            }
        }
        Bin bin = bins[binIndex];
        nativeAdWidget.transform.SetParent(bin.transform);

        int blockCount = bin.GetBlockCount();
        int randomInBlocks = UnityEngine.Random.Range(0, blockCount + 1);
        nativeAdWidget.GetComponent<RectTransform>().SetSiblingIndex(randomInBlocks + 1);
    }
}
